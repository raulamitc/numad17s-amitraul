package edu.neu.madcourse.amitraul.scroggle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.utility.Globals;


public class ScroggleGameActivity extends AppCompatActivity {
    public static final String KEY_RESTORE = "key_restore";
    public static final String PREF_RESTORE = "pref_restore";
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private ScroggleGameFragment mGameFragment;
    protected static HashSet<Long> wordsLong;
    protected static HashSet<String> wordsStr;
    protected static HashSet<Integer> wordsInt;
    private static ArrayList<String> nineLetterWords;
    private static String[] patterns = {"036785214","036785241","214587630","254103678",
                                        "043678521","630124785","031467852","036784512",
                                        "036478512","401367852","425103678","748521036","037852146"};
    private TextView timerValue;
    private Long timeInMilliseconds=0L;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long updatedTime = 0L;
    protected static long timeSwapBuff = 0L;
    protected static boolean isNewGame= true;
    private boolean gamePaused=false;
    private boolean MUSIC_PAUSED=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDictionary();
        setContentView(R.layout.activity_scroggle_game);
        mGameFragment = (ScroggleGameFragment) getFragmentManager()
                .findFragmentById(edu.neu.madcourse.amitraul.R.id.fragment_game);
        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);
        if (restore) {
                isNewGame=false;
                timerValue = (TextView) findViewById(R.id.timerValue);
//            timeSwapBuff = Long.parseLong(timerValue.getText().toString())*1000;
                String gameData = getPreferences(MODE_PRIVATE)
                        .getString(PREF_RESTORE, null);
                if (gameData != null) {
                    mGameFragment.putState(gameData);
                }
            }else{
                isNewGame=true;
                ScroggleGameFragment.round_Two=false;
                timeSwapBuff=0L;
                String gameData= initializeBoard();
                if (gameData != null) {
                    mGameFragment.putState(gameData);
                }
            }


        Button btn1= (Button)findViewById(R.id.done_button);
        Button clearBtn = (Button)findViewById(R.id.clear_button);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.current_word);
                tv.setText("");
                mGameFragment.failedWord();
            }});
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.current_word);
                String word= tv.getText().toString();
                tv.setText("");
                int l= word.length();
                if (l <= 6) {
                    int queryInt = 0;
                    for (char c : word.toCharArray()) {
                        queryInt += c - 96;
                        queryInt = queryInt << 5;
                    }
                    if (ScroggleGameActivity.wordsInt.contains(queryInt)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }

                } else if (l <= 12&& l!=9) {
                    long queryLong = 0;
                    for (char c : word.toCharArray()) {
                        queryLong += c - 96;
                        queryLong = queryLong << 5;
                    }
                    if (ScroggleGameActivity.wordsLong.contains(queryLong)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }
                }else{
                    if (ScroggleGameActivity.wordsStr.contains(word)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }
                }
                Log.d("scrog-Game-frag","button clicked");
            }
        });
        timerValue = (TextView) findViewById(R.id.timerValue);
        Log.d("UT3", "restore = " + restore);
        if(isNewGame){
            startTimer(ScroggleGameFragment.MAX_TIME);
        }else{
            startTimer(Globals.currentTime);
        }
    }

    private boolean turnedOff=false;
    Runnable updateTimerThread = new Runnable() {

             public void run() {
/*                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;

                int secs = (int) (updatedTime / 1000);*/

                    timerValue.setText(String.format("%02d", Globals.currentTime));
                    if (Globals.currentTime == 0) {
                        System.out.println("timer reached zero");
                        mGameFragment.startNextRound();
                        return;
                    }
                    if (Globals.currentTime <= 4) {
                        timerValue.setTextColor(getResources().getColor(R.color.red_color));
                    } else {
                        timerValue.setTextColor(getResources().getColor(R.color.white_color));
                    }
                    Globals.currentTime--;
                    customHandler.postDelayed(this, 1000);

             }

        };

    public void beep() {
        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
    }


    protected void loadDictionary() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    wordsInt = Globals.getWordsInt(getResources().openRawResource(R.raw.wordlist_en_int));
                    wordsLong = Globals.getWordsLong(getResources().openRawResource(R.raw.wordlist_en_long8));
                    wordsStr = Globals.getWords(getResources().openRawResource(R.raw.wordlist_en_str9));
                    nineLetterWords=Globals.getNineLetterWords();
//                    Snackbar.make(findViewById(android.R.id.content),
//                            "dictionary loaded",
//                            Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Snackbar.make(findViewById(android.R.id.content),
                            "restart the app please!",
                            Snackbar.LENGTH_SHORT).show();
                }
            }
        }).start();
    }

    private String initializeBoard() {
        StringBuilder sb = new StringBuilder();
        while(nineLetterWords==null){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            nineLetterWords=Globals.getNineLetterWords();
        }
        int l =nineLetterWords.size();
        sb.append("-1,-1,0,");
        Random rand = new Random();
        for(int i=0;i<9;i++){
            String word=nineLetterWords.get(rand.nextInt(l));
            char[] mat= getScrambledMatrix(word,rand);
            for(char c: mat){
                sb.append("NEITHER,");
                sb.append(c);
                sb.append(",");
            }
            System.out.println(word);
        }
       return  sb.toString();
    }

    protected char[] getScrambledMatrix(String word, Random rand) {
        char[] wordArr= new char[9];

        String pattern= patterns[rand.nextInt(patterns.length)];
        char[] charArray= pattern.toCharArray();
        int flag = rand.nextInt(2);
        for(int i=0; i<9;i++){
            if(flag==1) wordArr[charArray[i]-'0']=word.charAt(i);
            else wordArr[8-(charArray[i]-'0')]=word.charAt(i);
        }
        return wordArr;
    }

    public void restartGame() {
        mGameFragment.restartGame();
    }

    public void reportWinner(final ScroggleTile.Owner winner) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
        builder.setMessage(getString(R.string.declare_winner, winner));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        final Dialog dialog = builder.create();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMediaPlayer = MediaPlayer.create(ScroggleGameActivity.this,
                        winner == ScroggleTile.Owner.X ? R.raw.oldedgar_winner
                                : winner == ScroggleTile.Owner.O ? R.raw.notr_loser
                                : R.raw.department64_draw
                );
                mMediaPlayer.start();
                dialog.show();
            }
        }, 500);

        // Reset the board to the initial position
        mGameFragment.initGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = MediaPlayer.create(this, R.raw.scroggle_game_play);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
        if (gamePaused) pauseGame();
    }



    @Override
    protected void onPause() {
        super.onPause();
        pauseTimer();
        gamePaused=true;
        mHandler.removeCallbacks(null);
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        String gameData = mGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .commit();
        Log.d("UT3", "state = " + gameData);
    }


    public void startTimer(int time) {
        Globals.currentTime=time;
        pauseTimer();
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }
    public void pauseTimer(){
        //timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
    }

    public void pauseMusic() {
        if(mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            MUSIC_PAUSED=true;
        }
        else {
            mMediaPlayer.start();
            MUSIC_PAUSED=false;
        }
    }

    public void pauseGame() {
        pauseTimer();
        if(MUSIC_PAUSED && mMediaPlayer.isPlaying())  mMediaPlayer.pause();
        pauseMusic();
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton("Resume",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startTimer(Globals.currentTime);
                        if(MUSIC_PAUSED && mMediaPlayer.isPlaying()) mMediaPlayer.start();;
                    }
                });
        builder.show();
    }
}
