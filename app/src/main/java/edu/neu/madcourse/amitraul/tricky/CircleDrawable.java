package edu.neu.madcourse.amitraul.tricky;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by raula on 4/6/2017.
 */

public class CircleDrawable extends Drawable {
    private final int startIndex;
    private final ArrayList<Accelerometer> accData;

    public CircleDrawable(ArrayList<Accelerometer> accData, int i) {
        this.accData= accData;
        this.startIndex=i;
    }

    @Override
    public void draw(Canvas canvas) {
        int s =2;
        Paint myPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        myPaint.setStyle(Paint.Style.STROKE);
        for (int i = 0; i < 199; i++) {
            int w= s+2*i;
            float radius=(float) (0.5*(w+w)*0.5);
            int strokeWidth=2;  // or whatever
            myPaint.setStrokeWidth(strokeWidth);
            myPaint.setColor(accData.get(startIndex+i).getRGBHex());
            canvas.drawCircle(200, 200, radius, myPaint);
        }
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}
