package edu.neu.madcourse.amitraul.finalApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.util.TerritoryAdapter;

public class MyTerritoriesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private TerritoryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_territories);

        recyclerView = (RecyclerView) findViewById(R.id.territories_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new TerritoryAdapter(MenuMapActivity.CURRENT_USER);
        recyclerView.setAdapter(mAdapter);

    }
}
