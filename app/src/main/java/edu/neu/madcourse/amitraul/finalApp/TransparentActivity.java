package edu.neu.madcourse.amitraul.finalApp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.tutorial.CircleTutorialActivity;
import edu.neu.madcourse.amitraul.tricky.CircleDrawable;

public class TransparentActivity extends AppCompatActivity {
    ArrayList<Integer> images= new ArrayList<>(Arrays.asList( new Integer[] {R.id.trimage1,R.id.trimage2,R.id.trimage3,R.id.trimage4}));
    private Integer index;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparent);
        index=(Integer) getIntent().getExtras().get("index");
        for(final int image: images) {
            if(image==images.get(index)){
                ((ImageView)findViewById(image)).setImageResource(R.drawable.circle_tut);
            }
            ImageView button = (ImageView) findViewById(image);
            button.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
                @Override
                public void onClick(View v) {
                    if(v.getId()==images.get(index)){
                        CircleTutorialActivity.tileNum++;

                        if(CircleTutorialActivity.tileNum==2){
                            CircleTutorialActivity.tileNum=0;
                        }
                        finish();
                    }
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to quit the game?");
        builder.setCancelable(false);
        builder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mDialog.dismiss();
                    }
                });
        builder.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finishAffinity();
                        }
                        System.exit(0);
                    }
                });
        mDialog = builder.show();
    }

}
