package edu.neu.madcourse.amitraul.communication.util;

/**
 * Created by raula on 3/11/2017.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.OfflineGameTestActivity;
import edu.neu.madcourse.amitraul.communication.SelectFriends;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.scroggle.ScroggleGameActivity;

import static edu.neu.madcourse.amitraul.communication.SelectFriends.SERVER_KEY;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {
//    private  TextView txtHeader;
    private ArrayList<String> mDataset;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) v.findViewById(R.id.firstLine);
        }
    }

    public void add(int position, String item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FriendListAdapter(ArrayList<String> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FriendListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list, parent, false);
        context=parent.getContext();
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final String name = mDataset.get(position);
        holder.txtHeader.setText(mDataset.get(position));
        holder.txtHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                DatabaseReference mdataref = FirebaseDatabase.getInstance().getReference("data").child("users").child(holder.txtHeader.getText().toString());
                mdataref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
//                        pushNotificationNewThread(user.getToken());
                        Intent intent = new Intent(v.getContext(),OfflineGameTestActivity.class);
                        intent.putExtra("turns","10");
                        intent.putExtra("first",FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""));
                        intent.putExtra("second",holder.txtHeader.getText().toString());
                        FirebaseDatabase.getInstance().getReference("data").child("game")
                                .child(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]","")
                                +holder.txtHeader.getText()).child("current_player").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail());
                        context.startActivity(intent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                final DatabaseReference dataref = FirebaseDatabase.getInstance().getReference().child("data");
                dataref.child("game").child("currentUser").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""));
//                remove(name);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

/*    public void pushNotificationNewThread(final String clientToken) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification(clientToken);
            }
        }).start();
    }
    private void pushNotification(String clientToken) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Scroggle");
            jNotification.put("body", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has invited you to play scroggle");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "AsyncGameActivity");

            // If sending to a single client
            jPayload.put("to", clientToken);
            JSONObject data = new JSONObject();
            data.put("game_id",FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]","")
            +txtHeader.getText());
            data.put("first",FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""));
            data.put("second",txtHeader.getText().toString());
            data.put("turns",10);
            jPayload.put("data",data);

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("in friend list adapter", "run: " + resp);
                    Toast.makeText(txtHeader.getContext(),resp,Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }*/
}