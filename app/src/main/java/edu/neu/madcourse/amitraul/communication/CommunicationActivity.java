package edu.neu.madcourse.amitraul.communication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.utility.Globals;

public class CommunicationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth=FirebaseAuth.getInstance();
        setContentView(R.layout.activity_communication);

        View ackButton= findViewById(R.id.ack_button);
        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CommunicationActivity.this);
                Spanned message= Html.fromHtml(getResources().getString(R.string.commHtmlAck));
                builder.setMessage(message);
                builder.setCancelable(false);
                builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                AlertDialog mDialog = builder.show();
            }
        });

        View aboutButton= findViewById(R.id.about_button);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CommunicationActivity.this);
//                Spanned message= Html.fromHtml(getResources().getString(R.string.commHtmlAck));
                builder.setMessage(R.string.AboutTheGame);
                builder.setCancelable(false);
                builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                AlertDialog mDialog = builder.show();
            }
        });
    }


    public void signOut(View view){
        mAuth.signOut();
        SignInActivity.shouldSignOut=true;
        finish();
    }

    public void selectFriends(View view){
        Intent intent = new Intent(this,SelectFriends.class);
        startActivity(intent);
    }


}
