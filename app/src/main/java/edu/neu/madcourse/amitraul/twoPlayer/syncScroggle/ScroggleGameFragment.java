package edu.neu.madcourse.amitraul.twoPlayer.syncScroggle;


import android.app.Fragment;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.utility.Globals;

public class ScroggleGameFragment extends Fragment {
    private static final int SECOND_ROUND_TIME = 30;
    public static int MAX_TIME = 90 ;
    private int mSoundX, mSoundO, mSoundMiss, mSoundRewind;
    private SoundPool mSoundPool;
    private static HashMap<Character,Integer> tileScores;
    private ScroggleTile mEntireBoard = new ScroggleTile(this);
    private ScroggleTile mLargeTiles[] = new ScroggleTile[9];
    private ScroggleTile mSmallTiles[][] = new ScroggleTile[9][9];
    static protected int mLargeIds[] = {R.id.large1, R.id.large2, R.id.large3,
            R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
            R.id.large9,};
    static private int mSmallIds[] = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};
    static protected String currentWord="";
    private float mVolume = 1f;
    private Set<ScroggleTile> mAvailable = new HashSet<ScroggleTile>();
    private int mLastLarge;
    private int mLastSmall;
    static protected int currentlargeTile=-1;
    private android.app.AlertDialog mDialog;
    private int score=0;
    private boolean needRefresh;
    private ArrayList<String> allWords= new ArrayList<>();
    private TextView tvAllWords;
    private int oLastLarge;
    private int oLastSmall;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        tileScores= Globals.getTileScore();
        initGame();
        mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        mSoundX = mSoundPool.load(getActivity(), R.raw.sergenious_movex, 1);
        mSoundO = mSoundPool.load(getActivity(), R.raw.sergenious_moveo, 1);
        mSoundMiss = mSoundPool.load(getActivity(), R.raw.erkanozan_miss, 1);
        mSoundRewind = mSoundPool.load(getActivity(), R.raw.joanne_rewind, 1);
    }

    public void initGame() {
        Log.d("UT3", "init game");
//        round_Two=false;// TODO initGame called for new and continue game
        mEntireBoard = new ScroggleTile(this);
        Random rand = new Random();
        ArrayList<String> nineLetterWords = Globals.getNineLetterWords();
        int arrSize= nineLetterWords.size();
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large] = new ScroggleTile(this);
            String word= nineLetterWords.get(rand.nextInt(arrSize));
            char[] charArray= ((ScroggleGameActivity)getActivity()).getScrambledMatrix(word,rand);
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small] = new ScroggleTile(this);
                mSmallTiles[large][small].text= ""+ charArray[small];
            }
            mLargeTiles[large].setSubTiles(mSmallTiles[large]);
        }
        mEntireBoard.setSubTiles(mLargeTiles);

//         If the player moves first, set which spots are available
        mLastSmall = -1;
        mLastLarge = -1;
        oLastSmall = -1;
        oLastLarge = -1;
        setAvailableFromLastMove(mLastSmall,mLastLarge);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =
                inflater.inflate(R.layout.scroggle_large_board, container, false);
        initViews(rootView);
        updateAllTiles();
        return rootView;
    }

    private void initViews(View rootView) {
        mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            View outer = rootView.findViewById(mLargeIds[large]);
            mLargeTiles[large].setView(outer);

            for (int small = 0; small < 9; small++) {
                Button inner = (Button) outer.findViewById
                        (mSmallIds[small]);
                final int fLarge = large;
                final int fSmall = small;
                final ScroggleTile smallTile = mSmallTiles[large][small];
                smallTile.setView(inner);
                // ...
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        smallTile.animate();
                        // ...
                        if (isAvailable(smallTile)&&
                                (smallTile.getOwner()!= ScroggleTile.Owner.O&&smallTile.getOwner()!= ScroggleGameActivity.CURRENT_PLAYER)) {
                            mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                            makeMove(fLarge,fSmall);
                        } else {
                            mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                        }
                    }
                });
            }
        }
    }

    public void wordSuccess(){
        if(allWords.contains(currentWord)) {
            failedWord();
            return;
        }
        allWords.add(currentWord);
        tvAllWords = (TextView)getActivity().findViewById(R.id.all_words);
        String words="";
        for(String word:allWords){
            words+=word +" ";
        }
        tvAllWords.setText(words);
        updateScore(currentWord);
        resetWord();
        if(!needRefresh) {
            return;
        }
        needRefresh=false;
        mLargeTiles[mLastLarge].setOwner(ScroggleGameActivity.CURRENT_PLAYER);
        for(int i=0;i<mSmallTiles.length;i++){
            if(mSmallTiles[mLastLarge][i].getOwner()!= ScroggleGameActivity.CURRENT_PLAYER)
                mSmallTiles[mLastLarge][i].setOwner(ScroggleTile.Owner.O);
        }
        setAllAvailable();
        updateAllTiles();
        updateCloud(getState());
    }

    private void updateScore(String currentWord) {
        int currScore=0;
        if(getActivity()==null) return;
        for (char c: currentWord.toCharArray()) {
            currScore+=tileScores.get(Character.toUpperCase(c));
        }
        score += currScore*currentWord.length();
        ((TextView) getActivity().findViewById(R.id.scoreValue)).setText("Score: "+score);
        Log.d("Score",""+score);
    }

    private void makeMove(int large, int small) {
        if(large==oLastLarge) return;
        mLastLarge = large;
        mLastSmall = small;
        needRefresh=true;
        ScroggleTile smallTile = mSmallTiles[large][small];
        ScroggleTile largeTile = mLargeTiles[large];
        currentWord+=smallTile.text;
        TextView tv = (TextView) getActivity().findViewById(R.id.current_word);
        tv.setText(currentWord);
        smallTile.setOwner(ScroggleGameActivity.CURRENT_PLAYER);
        setAvailableTiles(small,large);
        String gameData= getState();
        updateCloud(gameData);
        updateAllTiles();
    }

    public boolean isAvailable(ScroggleTile tile) {
        return mAvailable.contains(tile);
    }

    public void putState(String gameData) {
        String[] fields = gameData.split(",");
        int index = 0;
        int[] coordinates= new int[4];
        for(int i=0; i<4;i++){
            coordinates[i]=Integer.parseInt(fields[index++]);
        }
        mLastLarge = coordinates[ScroggleGameActivity.PLAYER_VALUE*2];
        mLastSmall = coordinates[1+ScroggleGameActivity.PLAYER_VALUE*2];
        oLastLarge = coordinates[((1+ScroggleGameActivity.PLAYER_VALUE)%2)*2];
        oLastSmall = coordinates[1+((1+ScroggleGameActivity.PLAYER_VALUE)%2)*2];
//        Integer.parseInt(fields[index++]);
        ScroggleTile.Owner opponent= ScroggleGameActivity.CURRENT_PLAYER == ScroggleTile.Owner.X?ScroggleTile.Owner.Z:ScroggleTile.Owner.X;
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].setOwner(ScroggleTile.Owner.valueOf(fields[index++]));
                mSmallTiles[large][small].text=fields[index++];
            }
        }
        setAvailableTiles(mLastSmall,mLastLarge);
        updateScore("");
        updateAllTiles();
    }

    public void updateState(String gameData) {
        String[] fields = gameData.split(",");
        int index = 0;
        int[] coordinates= new int[4];
        for(int i=0; i<4;i++){
            coordinates[i]=Integer.parseInt(fields[index++]);
        }
        mLastLarge = coordinates[ScroggleGameActivity.PLAYER_VALUE*2];
        mLastSmall = coordinates[1+ScroggleGameActivity.PLAYER_VALUE*2];
        oLastLarge = coordinates[((1+ScroggleGameActivity.PLAYER_VALUE)%2)*2];
        oLastSmall = coordinates[1+((1+ScroggleGameActivity.PLAYER_VALUE)%2)*2];
//        Integer.parseInt(fields[index++]);
        ScroggleTile.Owner opponent= ScroggleGameActivity.CURRENT_PLAYER == ScroggleTile.Owner.X?ScroggleTile.Owner.Z:ScroggleTile.Owner.X;
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                if(large==mLastLarge){
                    index+=2;
                    continue;
                }
                mSmallTiles[large][small].setOwner(ScroggleTile.Owner.valueOf(fields[index++]));
                mSmallTiles[large][small].text=fields[index++];
            }
        }
        setAvailableTiles(mLastSmall,mLastLarge);
        updateScore("");
        updateAllTiles();
    }

    private void updateAllTiles() {
        mEntireBoard.updateDrawableState();
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large].updateDrawableState();
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].updateDrawableState();
            }
        }
    }

    private void setAvailableTilesRoundTwO(int large) {

        for(int s=0; s<mSmallTiles.length;s++){
            if(mAvailable.contains(mSmallTiles[large][s])) mAvailable.remove(mSmallTiles[large][s]);
        }
    }

    private void setAvailableFromLastMove(int small,int large) {
//        clearAvailable();
        // Make all the tiles at the destination available
        if (small != -1) {
            for (int dest = 0; dest < 9; dest++) {
                ScroggleTile tile = mSmallTiles[large][dest];
                if (tile.getOwner() == ScroggleTile.Owner.NEITHER)
                    addAvailable(tile);
            }
        }else{
            setAllAvailable();
        }
        // If there were none available, make all squares available
//        if (mAvailable.isEmpty()) {
//            setAllAvailable();
//        }
    }

    private void setAvailableTiles(int small, int large) {
        clearAvailable();
        // Make all the tiles at the destination available
        if (small != -1) {
            for (int i = -1+small/3; i <= 1+small/3; i++) {
                for (int j = -1+small%3; j <= 1+small%3; j++){
                    if ((i==small/3) && (j==small%3) || i<0 ||i>2||j<0||j>2 )  continue;
                    ScroggleTile tile = mSmallTiles[large][3*i+j];
                    if (tile.getOwner() == ScroggleTile.Owner.NEITHER)
                        addAvailable(tile);
                    }
            }
        }
        // If there were none available, make all squares available
        if (mAvailable.isEmpty()) {
            setAllAvailable();
        }
    }

    void clearAvailable() {
        mAvailable.clear();
    }
    private void addAvailable(ScroggleTile tile) {
        tile.animate();
        mAvailable.add(tile);
    }

    private void setAllAvailable() {
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                ScroggleTile tile = mSmallTiles[large][small];
                if (tile.getOwner() == ScroggleTile.Owner.NEITHER)
                    addAvailable(tile);
            }
        }
    }
    public void restartGame() {
        mSoundPool.play(mSoundRewind, mVolume, mVolume, 1, 0, 1f);
        score=0;
        allWords.clear();
        tvAllWords= (TextView)getActivity().findViewById(R.id.all_words);
        tvAllWords.setText("");

        updateScore("");
        resetWord();
        ((ScroggleGameActivity)getActivity()).startTimer(MAX_TIME);
        initGame();
        initViews(getView());
        updateAllTiles();
    }

    public String getState() {
        StringBuilder builder = new StringBuilder();
        if(ScroggleGameActivity.PLAYER_VALUE==1){
            builder.append(oLastLarge);
            builder.append(',');
            builder.append(oLastSmall);
            builder.append(',');
        }
        builder.append(mLastLarge);
        builder.append(',');
        builder.append(mLastSmall);
        builder.append(',');
        if(ScroggleGameActivity.PLAYER_VALUE==0){
            builder.append(oLastLarge);
            builder.append(',');
            builder.append(oLastSmall);
            builder.append(',');
        }
    /*    builder.append(score);
        builder.append(',');*/
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                builder.append(mSmallTiles[large][small].getOwner().toString());
                builder.append(',');
                builder.append(mSmallTiles[large][small].text);
                builder.append(',');
            }
        }
        String gameData= builder.toString();

        return builder.toString();
    }

    private void resetWord(){
        currentWord="";
        TextView tv = (TextView) getActivity().findViewById(R.id.current_word);
        tv.setText(currentWord);
    }
    public void failedWord() {
        resetWord();
 /*       if(!needRefresh) {
            return;
        }
        needRefresh=false;*/
        for(int i=0; i<mSmallTiles.length;i++){
            mSmallTiles[mLastLarge][i].setOwner(ScroggleTile.Owner.NEITHER);
//            mSmallTiles[mLastLarge][i].updateDrawableState();
//            setAllAvailable();
            addAvailable(mSmallTiles[mLastLarge][i]);
        }
        updateAllTiles();

        updateCloud(getState());
    }

    public void updateCloud(String gameData){
        ((ScroggleGameActivity)getActivity()).gameIdref.child("gameData").setValue(gameData);
    }

    public void displayScore() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setMessage(Html.fromHtml("Congrats!! <br/> <b> Your Score is "+score+"</b><br/><br/>" /*+
                "Do you want to restart the game?"*/));
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().finish();
                    }
                });
        mDialog = builder.show();
        ((ScroggleGameActivity)getActivity()).dataref.child("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-z0-9A-Z]",""))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        int currScore = Integer.parseInt(user.getScore()==null||user.getScore()==""?"0":user.getScore());
                        user.setScore(""+Math.max(currScore,score));
                        dataSnapshot.getRef().setValue(user);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        // Update the Hscore of current user

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void rotateAllTiles(int rotation){
        for (int large=0; large<9;large++){
            for (int small=0; small<9;small++){
                mSmallTiles[large][small].rotateTile(rotation);
            }
        }
    }
}
