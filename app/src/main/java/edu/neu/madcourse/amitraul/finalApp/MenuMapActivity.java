package edu.neu.madcourse.amitraul.finalApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.firebaseAuth.ConnectToFirebaseDatabase;
import edu.neu.madcourse.amitraul.finalApp.model.User;
import edu.neu.madcourse.amitraul.tricky.CircleActivity;

public class MenuMapActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback, View.OnClickListener {
    public static HashMap<String, String> ALL_TERRITORIES = null;
    public static User CURRENT_USER = null;
    public static String CURRENT_USER_ID = null;
    private GoogleMap map;
    MapView mapView;
    // NEU - 42.339973, -71.088935
    public static double lat = 42.339973;
    public static double lon = -71.088935;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    public static boolean shouldSignOut = false;
    private static boolean asyncPlayer = false;
    private FirebaseAuth mAuth;
    // [START declare_auth_listener]
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference database;
    private int opponentId;
    private ArrayList<Circle> circle_list = new ArrayList<>();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    //Multiplayer
    private static User opponentPlayer = null;
    private String opponentToken;
    private String currentPlayerTerritoryOnRisk = "";
    private String opponentPlayerTerritoryOnRisk = "";
    private DialogInterface mDialog;
    private ValueEventListener connectionValueListener;
    private DatabaseReference connectedRef;
    private boolean dataLoaded=false;
    private boolean territoriesLoaded=false;
    private boolean userLoaded=false;

    TextView googleSignInDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_map);
        googleSignInDescription = (TextView) findViewById(R.id.googleSignInDescription);
        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        if (!PreferenceManager.getDefaultSharedPreferences(getBaseContext()).contains("Circlefirst")) {
          /*  new AlertDialog.Builder(MenuMapActivity.this)
                    .setTitle("Google Sign in")
                    .setMessage("Please Sign in with Google to play circles so that you can build your own territories and capture more territories")
                    .setCancelable(false)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();*/
            PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putInt("Circlefirst", 0).commit();
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        dataLoaded=false;
        DatabaseReference ref = ConnectToFirebaseDatabase.instance(getApplicationContext()).getReference();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Button listeners
        findViewById(R.id.sign_in_btn).setOnClickListener(this);
        findViewById(R.id.sign_out_btn).setOnClickListener(this);


        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                updateUI(user);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if (mGoogleApiClient.isConnected() && shouldSignOut) {
            signOut();
            shouldSignOut = false;
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("game_id")) {
            asyncPlayer = true;
        } else {
            asyncPlayer = false;
        }

        connectionValueListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    System.out.println("connected");
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuMapActivity.this);
                    builder.setMessage("Trying to connect to Internet. Check if your internet is working correctly.Click OK if everything is working fine");
                    builder.setCancelable(false);
                    builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // nothing
                                }
                            });
                    try {AlertDialog mDialog = builder.show();}
                    catch (Exception e) {e.printStackTrace();}
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }};

        connectedRef.addValueEventListener(connectionValueListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            connectedRef.removeEventListener(connectionValueListener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    // [START on_start_add_listener]
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    // [END on_stop_remove_listener]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        signOut();
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                updateUI(null);
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        signOut();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MenuMapActivity.this);
                            builder.setMessage("Not able to sign in. " +
                                    "Please Quit the game and Sign in again.");
                            builder.setCancelable(false);
                            builder.setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                                finishAffinity();
                                            }
                                            System.exit(0);
                                        }
                                    });
                            mDialog = builder.show();
                        } else {
                            updateUser(mAuth.getCurrentUser());
                        }
                    }
                });
    }

    private void updateUser(FirebaseUser user) {
        DatabaseReference usersRef = database.child("users");
//        usersRef.addListenerForSingleValueEvent();
        String userId = user.getEmail().replaceAll("[^a-zA-Z0-9]", "");
        CURRENT_USER_ID = userId;
        usersRef.child(userId).child("email").setValue(user.getEmail());
        usersRef.child(userId).child("name").setValue(user.getDisplayName());
        usersRef.child(userId).child("token").setValue(FirebaseInstanceId.getInstance().getToken());
        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CURRENT_USER = dataSnapshot.getValue(User.class);
                CURRENT_USER.setMyTerritories((HashMap<String, String>) dataSnapshot.child("territories").getValue());
                Log.d(TAG,"user signed in");
                findViewById(R.id.my_territory).setVisibility(View.VISIBLE);
                userLoaded=true;

                dataLoaded=territoriesLoaded&&userLoaded;
                drawCirclesOnMap();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        database.child("territories").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ALL_TERRITORIES = (HashMap<String, String>) dataSnapshot.getValue();
                territoriesLoaded=true;
                dataLoaded=territoriesLoaded&&userLoaded;
                drawCirclesOnMap();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//        if(!getPreferences(MODE_PRIVATE).contains("score")) getPreferences(MODE_PRIVATE).edit().putInt("score",0);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        updateUI(null);
                    }
                });
    }

    private void revokeAccess() {
        mAuth.signOut();
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        updateUI(null);
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {

            findViewById(R.id.sign_in_btn).setVisibility(View.GONE);
            googleSignInDescription.setVisibility(View.GONE);
            findViewById(R.id.play_game_layout).setVisibility(View.VISIBLE);
            updateUser(user);
        } else {

            findViewById(R.id.sign_in_btn).setVisibility(View.VISIBLE);
            googleSignInDescription.setVisibility(View.VISIBLE);
            findViewById(R.id.play_game_layout).setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
        setLocationOnMap(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.sign_in_btn) {
            signIn();
        } else if (i == R.id.sign_out_btn) {
            signOut();
        }
    }

    private void drawCirclesOnMap() {

        /*Iterate through  my terrotories ans place each green circle
        * */
        map.clear();
        for (Circle c : circle_list) {
            c.remove();
        }
        circle_list.clear();
        // update all circles by removing and replacing them
        // maintain a pointer in a hashmap that can be access through a LatLang key

        if(ALL_TERRITORIES == null || CURRENT_USER == null || !dataLoaded) return;
        if (CURRENT_USER.getMyTerritories()==null) CURRENT_USER.setMyTerritories(new HashMap<String, String>());
        for (String key : ALL_TERRITORIES.keySet()) {
            String dkey = key.replaceAll("[|]", ".");
            double klat = Double.parseDouble(dkey.split("_")[0]);
            double klon = Double.parseDouble(dkey.split("_")[1]);

            // Get current user radius
            String value[] = ALL_TERRITORIES.get(key).split("[|]");
            double rad = 0.0;
            if (value.length > 0) {
                rad = Double.parseDouble(value[0]);
            }

            if (!territoryOverlapped(new LatLng(klat, klon), new LatLng(lat, lon), 50000, 0))
                continue; // skip territories outside 5 km

            if (CURRENT_USER.getMyTerritories().containsKey(key)) {
                //Green circle
                Circle c = map.addCircle(new CircleOptions().center(new LatLng(klat, klon)).radius(rad).strokeColor(Color.BLACK));
                c.setFillColor(getResources().getColor(R.color.green_color));
                circle_list.add(c);
            } else {
                // Red Circle
                Circle c = map.addCircle(new CircleOptions().center(new LatLng(klat, klon)).radius(rad).strokeColor(Color.BLACK).clickable(true));
                c.setFillColor(getResources().getColor(R.color.red_color));
                circle_list.add(c);
            }
        }

        map.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                if (circle.isClickable()) {

                    // Get opponent latlong key
                    LatLng opponentCircle = circle.getCenter();
                    double opponentLatitude = opponentCircle.latitude;
                    double opponentLongitude = opponentCircle.longitude;
                    String opponentKey = ((String.valueOf(opponentLatitude)) + "_" + (String.valueOf(opponentLongitude))).replaceAll("[.]", "|");

                    // Get opponent id from ALL_TERRITORIES
                    if(!ALL_TERRITORIES.containsKey(opponentKey)) return;
                    String value[] = ALL_TERRITORIES.get(opponentKey).split("[|]");
                    String opponentEmailId = "";
                    if (value.length > 0) {
                        opponentEmailId = value[1];
                    }
                    getOpponentDetails(opponentEmailId, CURRENT_USER.getToken(), circle);
                }
            }
        });
    }

    // Fetching the opponent player details for multiplayer game
    public void getOpponentDetails(final String opponentEmailId, final String currentUserToken, final Circle circle) {
        database.child("users").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User opponent = (User) dataSnapshot.getValue(User.class);
                if (dataSnapshot.getKey().equals(opponentEmailId)) {
                    opponentPlayer = opponent;
                    for (String key : CURRENT_USER.getMyTerritories().keySet()) {
                        String dkey = key.replaceAll("[|]", ".");
                        double klat = Double.parseDouble(dkey.split("_")[0]);
                        double klon = Double.parseDouble(dkey.split("_")[1]);
                        double rad = 0.0;
                        String currentUserToken = "";
                        String currentUserValue[] = ALL_TERRITORIES.get(key).split("[|]");
                        if (currentUserValue.length > 0) {
                            rad = Double.parseDouble(currentUserValue[0]);
                        }

                        boolean overlap = territoryOverlapped(circle.getCenter(), new LatLng(klat, klon), circle.getRadius(), rad);
                        if (overlap) {

                            findViewById(R.id.conquerBtn).setVisibility(View.VISIBLE);

                            // GOAL: fetch opponentid and token
                            opponentToken = opponentPlayer.getToken();
                            currentPlayerTerritoryOnRisk = key;
                            opponentPlayerTerritoryOnRisk = (String.valueOf(circle.getCenter().latitude) + "_" + String.valueOf(circle.getCenter().longitude)).replaceAll("[.]", "|");

                            Toast.makeText(MenuMapActivity.this, "Click CONQUER TERRITORY!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            findViewById(R.id.conquerBtn).setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public boolean territoryOverlapped(LatLng hisCenter, LatLng myCenter, double hisRadius, double myRadius) {
        return calculateDistanceInKilometer(myCenter.latitude, myCenter.longitude, hisCenter.latitude, hisCenter.longitude) * 1000 <
                (hisRadius + myRadius);
    }

    public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;

    public double calculateDistanceInKilometer(double userLat, double userLng,
                                               double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return AVERAGE_RADIUS_OF_EARTH_KM * c;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_SHORT).show();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG,"Permission granted");
            setLocationOnMap(true);
        }
    }

    private void setLocationOnMap(boolean bUseSensor) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("Permission Denied");
            String str = "Permission DENIED";
            Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
            return;
        }
        map.setMyLocationEnabled(true);
        if (bUseSensor) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                //Getting longitude and latitude
                lon = location.getLongitude();
                lat = location.getLatitude();

            } else {
                LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch(Exception ex) {
                    ex.printStackTrace();
                }

                if(!gps_enabled && !network_enabled) {
                    // notify user to enable location services if location is null
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MenuMapActivity.this);
                    dialog.setMessage("Turn On Location Services to " +
                            "Allow \" Circles \"  to " +
                            "Determine Your Location" +
                            '\n' + "Your location will be used to build your " +
                            "territories and conquer other territories around you.");
                    dialog.setPositiveButton("Open Location Settings", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                        }
                    });
                    dialog.setNegativeButton("Maybe Later", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                finishAffinity();
                            }
                            System.exit(1);
                        }
                    });
                    dialog.show();
                }
            }
        }
        LatLng ll = new LatLng(lat,lon);
//        map.addMarker(new MarkerOptions().position(ll).title("Marker"));
        map.moveCamera(CameraUpdateFactory.newLatLng(ll));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setLocationOnMap(false);
        map.setBuildingsEnabled(true);
        drawCirclesOnMap();
    }

    public void quitApp(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to quit the game?");
        builder.setCancelable(false);
        builder.setPositiveButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mDialog.dismiss();
                    }
                });
        builder.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finishAffinity();
                        }
                        System.exit(0);
                    }
                });
        mDialog = builder.show();
    }

    public void conquerTerritory(View view) {
        Intent intent = new Intent(this, CircleActivity.class);
        intent.putExtra("mode", "Multiplayer");
        intent.putExtra("token", opponentToken);
        intent.putExtra("email", opponentPlayer.getEmail());
        intent.putExtra("opponentTerritory", opponentPlayerTerritoryOnRisk);
        intent.putExtra("currentPlayerEmail", CURRENT_USER.getEmail());
        intent.putExtra("currentPlayerToken", CURRENT_USER.getToken());
        intent.putExtra("currentPlayerTerritory", currentPlayerTerritoryOnRisk);
        //intent.putExtra("opponentId",opponentId);
        startActivity(intent);
    }

    public void buildTerritory(View view) {
        Intent intent = new Intent(this, CircleActivity.class);
        intent.putExtra("mode", "Singleplayer");
        startActivity(intent);
    }

    public void showMyTerritories(View view){
        Intent intent = new Intent(this, MyTerritoriesActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                    Intent intent = new Intent(this,MenuMapActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(this, "Need your location to play the game!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}