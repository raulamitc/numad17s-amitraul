package edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle.ScroggleGameFragment;

/**
 * Created by raula on 2/15/2017.
 */

public class ScroggleTile {


    public enum Owner {
        X, O , NEITHER, BOTH, Z
    }


    // These levels are defined in the drawable definitions
    private static final int LEVEL_X = 0;
    private static final int LEVEL_O = 1; // letter O
    private static final int LEVEL_BLANK = 2;
    private static final int LEVEL_AVAILABLE = 3;
    private static final int LEVEL_TIE = 3;
    private static final int LEVEL_Z = 4;
    public String text;

    private final edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle.ScroggleGameFragment mGame;
    private ScroggleTile.Owner mOwner = ScroggleTile.Owner.NEITHER;
    private View mView;
    private ScroggleTile mSubTiles[];

    public ScroggleTile(ScroggleGameFragment game) {
        this.mGame = game;
    }

    public ScroggleTile deepCopy() {
        ScroggleTile tile = new ScroggleTile(mGame);
        tile.setOwner(getOwner());
        if (getSubTiles() != null) {
            ScroggleTile newTiles[] = new ScroggleTile[9];
            ScroggleTile oldTiles[] = getSubTiles();
            for (int child = 0; child < 9; child++) {
                newTiles[child] = oldTiles[child].deepCopy();
            }
            tile.setSubTiles(newTiles);
        }
        return tile;
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public ScroggleTile.Owner getOwner() {
        return mOwner;
    }

    public void setOwner(ScroggleTile.Owner owner) {
        this.mOwner = owner;
    }

    public ScroggleTile[] getSubTiles() {
        return mSubTiles;
    }

    public void setSubTiles(ScroggleTile[] subTiles) {
        this.mSubTiles = subTiles;
    }

    public void updateDrawableState() {
        if (mView == null) return;
        int level = getLevel();
        if (mView.getBackground() != null) {
            mView.getBackground().setLevel(level);
        }
        if (mView instanceof Button) {
            ((Button) mView).setText(text);
//            drawable.setLevel(1);
        }
    }

    public void rotateTile(int rotation){
        if (mView==null) return;
        else{
            mView.setRotation(rotation);
        }
    }

/*    private char getChar(){

    }*/
    private int getLevel() {
        int level = LEVEL_BLANK;
        switch (mOwner) {
            case X:
                level = LEVEL_X;
                break;
            case O: // letter O
                level = LEVEL_O;
                break;
            case Z:
                level = LEVEL_Z;
                break;
            case BOTH:
                level = LEVEL_TIE;
                break;
            case NEITHER:
                level = mGame.isAvailable(this) ? LEVEL_AVAILABLE : LEVEL_BLANK;
                break;
        }
        return level;
    }



    public void animate() {
        if(mGame==null||mGame.getActivity()==null) return;
        Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
                edu.neu.madcourse.amitraul.R.animator.tictactoe);
        if (getView() != null) {
            anim.setTarget(getView());
            anim.start();
        }
    }
}
