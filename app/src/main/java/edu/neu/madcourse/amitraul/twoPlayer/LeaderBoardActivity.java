package edu.neu.madcourse.amitraul.twoPlayer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.twoPlayer.util.LeaderboardAdapter;

public class LeaderBoardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private LeaderboardAdapter mAdapter;
    private ArrayList<User> al = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        recyclerView = (RecyclerView) findViewById(R.id.leaderboard_friends_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("data");
        database.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    al.add(ds.getValue(User.class));
                }
                Collections.sort(al, new Comparator<User>() {
                    @Override
                    public int compare(User o1, User o2) {
                        int thisScore = Integer.parseInt(o1.getScore()==null||o1.getScore().equals("")?"0":o1.getScore());
                        int otherScore =Integer.parseInt(o2.getScore()==null||o2.getScore().equals("")?"0":o2.getScore());
                        if(thisScore>otherScore){
                            return -1;
                        }else if(otherScore>thisScore){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
                });

                mAdapter = new LeaderboardAdapter(al);
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
