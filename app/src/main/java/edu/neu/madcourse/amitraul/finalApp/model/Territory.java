package edu.neu.madcourse.amitraul.finalApp.model;

/**
 * Created by raula on 4/28/2017.
 */

public class Territory {
    private String location;
    private String radius;

    public String getLocation() {
        String loc = "Latitude: "+location.replaceAll("[|]",".").split("_")[0] +"\n"+"Longitude: "+location.replaceAll("[|]",".").split("_")[1];
        return loc;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
}
