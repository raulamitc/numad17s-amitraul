package edu.neu.madcourse.amitraul;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.amitraul.finalApp.CirclesMainActivity;
import edu.neu.madcourse.amitraul.finalApp.FinalAppAcknowledgements;

public class FinalAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_app);
    }

    public void openCircleApp(View view){
        Intent intent = new Intent(this, CirclesMainActivity.class);
        startActivity(intent);
    }

    public void showCirclesAcknowledgements(View view) {
        Intent intent = new Intent(this, FinalAppAcknowledgements.class);
        startActivity(intent);
    }

    public void quitCircles(View view) {
        finish();
    }
}
