package edu.neu.madcourse.amitraul.tricky;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;

import edu.neu.madcourse.amitraul.R;

import static android.widget.Toast.*;

public class MapActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback{

    private GoogleMap map;
    MapView mapView;
    // NEU - 42.339973, -71.088935
    private double lat = 42.339973;
    private double lon = -71.088935;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void drawCirclesOnMap() {
        map.addCircle(new CircleOptions().center(new LatLng(lat,lon)).radius(500.0).fillColor(R.color.red_color));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_SHORT).show();
        setLocationOnMap(true);
    }

    private void setLocationOnMap(boolean bUseSensor) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("Permission Denied");
            String str = "Permission DENIED";
            Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
            return;
        }
        map.setMyLocationEnabled(true);
        if(bUseSensor){
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                //Getting longitude and latitude
                lat = location.getLongitude();
                lon = location.getLatitude();

            }
            else{
                Toast.makeText(getBaseContext(), "LOCATION IS NULL", Toast.LENGTH_SHORT).show();
            }
        }
        LatLng ll = new LatLng(lat,lon);
        map.addMarker(new MarkerOptions().position(ll).title("Marker"));
        map.moveCamera(CameraUpdateFactory.newLatLng(ll));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getBaseContext(), "Not Connected", Toast.LENGTH_SHORT).show();
        setLocationOnMap(false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setLocationOnMap(false);
        map.setBuildingsEnabled(true);
        drawCirclesOnMap();
    }

    //    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        map = googleMap;
//        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
//            map.setMyLocationEnabled(true);
//        }
//        map.setTrafficEnabled(false);
//        map.setIndoorEnabled(false);
//        map.setBuildingsEnabled(true);
//        map.getUiSettings().setZoomControlsEnabled(true);
//
//        // Add a marker in Sydney and move the camera
//        LatLng myLoc = new LatLng(lat, lon);
//        map.addMarker(new MarkerOptions().position(myLoc).title("Marker is your location"));
//        map.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
//        map.animateCamera( CameraUpdateFactory.zoomTo( 10.0f ) );
//    }
}
