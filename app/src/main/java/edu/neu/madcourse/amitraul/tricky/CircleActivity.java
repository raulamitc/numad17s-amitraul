package edu.neu.madcourse.amitraul.tricky;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.MenuMapActivity;
import edu.neu.madcourse.amitraul.finalApp.model.ActivityRange;
import edu.neu.madcourse.amitraul.utility.Globals;

import static edu.neu.madcourse.amitraul.finalApp.MenuMapActivity.ALL_TERRITORIES;
import static edu.neu.madcourse.amitraul.finalApp.MenuMapActivity.CURRENT_USER;
import static edu.neu.madcourse.amitraul.finalApp.MenuMapActivity.lat;
import static edu.neu.madcourse.amitraul.finalApp.MenuMapActivity.lon;

public class CircleActivity extends AppCompatActivity {

    private ArrayList<Accelerometer> accData;
    public static final String SERVER_KEY = "key=AAAAd7PmPW4:APA91bGBLeoJFAcLQHh4utfL6BzpIX9kgneWcXL7ACwP_ATKCy5SZnUemsQooeiQisTBXxPsmK2k0WY0avFgw_luOwI0oh16PIGf-JLdxBhfTy9qpl89HahGDhr8zR3Pvkq_UkRIAv51";
    ArrayList<Integer> images= new ArrayList<>(Arrays.asList( new Integer[] {R.id.image1,R.id.image2,R.id.image3,R.id.image4}));
    HashMap<Integer,String> imageData=new HashMap<>();
    HashMap<String, ArrayList<ActivityRange>> annotations = new HashMap<>();
    List<String> keys;
    private String currLabel;
    private int progress;
    Button addTerritoryBtn;
    Button quitBtn;
    DatabaseReference mDatabase;

    // Multiplayer
    HashSet<String> newGame = new HashSet<String>();
    Iterator<String> iterator = null;
    private String mode;
    private String opponentPlayerId;
    private String opponentToken;
    private String opponentPlayerTerritoryOnRisk;

    private String currentPlayerToken;
    private String challengerPlayerId;
    private String currentPlayerTerritoryOnRisk;
    private int currentPlayerScore = 20;
    private int opponentPlayerScore = 0;

    private int[][] gameData= new int[20][4];
    private String[][] labelData= new String[20][4];
    private int mulTurn=0;
    private String gameId;
    private String mulData ="mulData";

    // value listener to listener to changes in a game
    private ValueEventListener gameValueListener= new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                HashMap<String, String> mp = (HashMap<String, String>) dataSnapshot.getValue();
                if (mp.containsKey("winner")) {
                    String winner = mp.get("winner");
                    String chalTer="challengerTerritory";
                    String oppId = ALL_TERRITORIES.get(mp.get("opponentTerritory")).split("[|]")[1];
                    String oppRad = ALL_TERRITORIES.get(mp.get("opponentTerritory")).split("[|]")[0];
                    String myId = ALL_TERRITORIES.get(mp.get(chalTer)).split("[|]")[1];
                    String myRad = ALL_TERRITORIES.get(mp.get(chalTer)).split("[|]")[0];
                    double newRad = Math.sqrt(Math.pow(Double.parseDouble(myRad), 2)
                            + Math.pow(Double.parseDouble(oppRad), 2));
                    DatabaseReference root = dataSnapshot.getRef().getRoot(); //TODO replace getParent with getRoot
                    if (winner.split("[|]")[1].equals("opponent")) {
                        ALL_TERRITORIES.put(mp.get("opponentTerritory"), "" + newRad + "|" + oppId);
                        ALL_TERRITORIES.remove(mp.get(chalTer));
                        CURRENT_USER.getMyTerritories().put("opponentTerritory", "" + newRad);
                        CURRENT_USER.getMyTerritories().remove(chalTer);

                        root.child("users").child(myId).child("territories")
                                .child(mp.get(chalTer)).setValue(null);
                        root.child("users").child(oppId).child("territories")
                                .child(mp.get("opponentTerritory")).setValue("" + newRad);

                        root.child("territories")
                                .child(mp.get(chalTer)).setValue(null);
                        root.child("territories")
                                .child(mp.get("opponentTerritory")).setValue("" + newRad + "|" + oppId);
                    } else {
                        ALL_TERRITORIES.put(mp.get(chalTer), "" + newRad);
                        ALL_TERRITORIES.remove(mp.get("opponentTerritory"));

                        CURRENT_USER.getMyTerritories().put(chalTer, "" + newRad);
                        CURRENT_USER.getMyTerritories().remove("opponentTerritory");

                        root.child("users").child(myId).child("territories")
                                .child(mp.get(chalTer)).setValue("" + newRad);
                        root.child("users").child(oppId).child("territories")
                                .child(mp.get("opponentTerritory")).setValue(null);

                        root.child("territories")
                                .child(mp.get("opponentTerritory")).setValue(null);
                        root.child("territories")
                                .child(mp.get(chalTer)).setValue("" + newRad + "|" + myId);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private boolean playerTwo=false;
    private int firstPlayerScore;
    private ValueEventListener connectionValueListener;
    private DatabaseReference connectedRef;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle);
        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        mDatabase = FirebaseDatabase.getInstance().getReference();

        progress = getPreferences(MODE_PRIVATE).getInt("progress", 0);
        ((ProgressBar) findViewById(R.id.territoryProgressBar)).setProgress(progress);
        initializeAnnotations();
        keys = new ArrayList<>(annotations.keySet());
        try {
            accData = Globals.getAccelerometerData(getResources().openRawResource(R.raw.accelerometer_value));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (getIntent().getExtras().containsKey("gameId")) {
            gameId = (String) getIntent().getExtras().get("gameId");
            playerTwo = true;
            findViewById(R.id.progressMessage).setVisibility(View.GONE);
            ((Button) findViewById(R.id.refreshData)).setText("Forfeit My Territory");
            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference(mulData).child(gameId);
            mode="Multiplayer";
            mulTurn=0;
            gameRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot==null) return;
                    HashMap<String, String> game = (HashMap<String, String>) dataSnapshot.getValue();
                    firstPlayerScore = Integer.parseInt(game.get("challengerScore"));
                    challengerPlayerId = game.get("challengerPlayerId");
                    opponentPlayerId = game.get("opponentPlayerId");
                    loadMultiplayerGame(game.get("gameData"), game.get("labelData"));
                    startMultiplayerGame();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return;
        }
        else{
        //Get mode
        mode = getIntent().getExtras().getString("mode");
        opponentPlayerId = getIntent().getExtras().getString("email");
        opponentToken = getIntent().getExtras().getString("token");
        opponentPlayerTerritoryOnRisk = getIntent().getExtras().getString("opponentTerritory");

        challengerPlayerId = getIntent().getExtras().getString("currentPlayerEmail");
        currentPlayerToken = getIntent().getExtras().getString("currentPlayerToken");
        currentPlayerTerritoryOnRisk = getIntent().getExtras().getString("currentPlayerTerritory");

        //Multiplayer game
        if (mode.equalsIgnoreCase("Multiplayer")) {
            initializeMul();
            findViewById(R.id.progressMessage).setVisibility(View.GONE);
            ((Button) findViewById(R.id.refreshData)).setText("Forfeit My Territory");

            String gameDataInString = "";
            String labelDataInString = "";
            for (int j = 0; j < 20; j++) {
                for (int k = 0; k < 4; k++) {
                    if (k == 3) {
                        gameDataInString = gameDataInString + gameData[j][k];
                        labelDataInString = labelDataInString + labelData[j][k];
                    } else {
                        gameDataInString = gameDataInString + gameData[j][k] + ",";
                        labelDataInString = labelDataInString + labelData[j][k] + ",";
                    }
                }
                if (j == 19) {
                    // do nothing
                } else {
                    gameDataInString = gameDataInString + ":";
                    labelDataInString = labelDataInString + ":";
                }
            }
            gameId = challengerPlayerId + "|" + opponentPlayerId;
            gameId = gameId.replaceAll("[^a-zA-Z0-9]", "");

//            FirebaseDatabase.getInstance().getReference(mulData).setValue(gameId);
            DatabaseReference gameRef = FirebaseDatabase.getInstance().getReference(mulData).child(gameId);
            gameRef.child("challengerTerritory").setValue(currentPlayerTerritoryOnRisk);
            gameRef.child("opponentTerritory").setValue(opponentPlayerTerritoryOnRisk);
            gameRef.child("challengerPlayerId").setValue(challengerPlayerId.replaceAll("[^a-zA-Z0-9]", ""));
            gameRef.child("opponentPlayerId").setValue(opponentPlayerId.replaceAll("[^a-zA-Z0-9]", ""));
            // Save game board
            gameRef.child("gameData").setValue(gameDataInString);
            gameRef.child("labelData").setValue(labelDataInString);
            gameRef.child("winner").setValue(null);
            gameRef.addValueEventListener(gameValueListener);
            // send notification to opponen
            startMultiplayerGame();

        }
        // Single player game
        else if (mode.equalsIgnoreCase("Singleplayer")) {
            loadGame();
        }
    }

        quitBtn = (Button) findViewById(R.id.refreshData);
        addTerritoryBtn = (Button) findViewById(R.id.addTerritory);

        // Quit button
        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode.equalsIgnoreCase("Multiplayer")){
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(CircleActivity.this);
                    dialog.setMessage("By clicking yes you would lose your hard earned territory.\n Are you sure?");
                    dialog.setPositiveButton("Keep Playing", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            mDialog.dismiss();
                        }
                    });
                    dialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            FirebaseDatabase.getInstance().getReference(mulData).child(gameId).child("winner").setValue(opponentPlayerId.replaceAll("[^a-zA-Z0-9]", "")+"|"+"opponent");
                            finish();
                        }
                    });
                    mDialog=dialog.show();

                }
                else finish();
            }
        });

        // Add territory button
        addTerritoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setting the progress back to zero
                progress=0;
                v.setVisibility(View.GONE);
                findViewById(R.id.progressMessage).setVisibility(View.VISIBLE);
                ((ProgressBar)findViewById(R.id.territoryProgressBar)).setProgress(progress);
                getPreferences(MODE_PRIVATE).edit().putInt("progress",progress).commit();
                HashMap<String,String> ter= MenuMapActivity.CURRENT_USER.getMyTerritories();

                for(String key: ter.keySet()) {
                   /*get current location
                   * if there exists a location in ter that we can fit into, expand territory and break
                   * Also update the global list of territories
                   * */
                    String dKey=key.replaceAll("[|]",".");
                    if (territoryOverlapped(new LatLng(Double.parseDouble(dKey.split("_")[0]), Double.parseDouble(dKey.split("_")[1])),
                            new LatLng(lat, lon), Double.parseDouble(ter.get(key)),0)) {
                        double radius = Math.sqrt(Math.pow(Double.parseDouble(ter.get(key)), 2) + Math.pow(100, 2));
                        Log.d("Radius", ""+radius);
                        ter.put(key, "" + radius);
                        FirebaseDatabase.getInstance().getReference("users").child(MenuMapActivity.CURRENT_USER_ID).child("territories").child(key).setValue(""+radius);

                        // Changing user id  instead of radius to be stored in territories
                        FirebaseDatabase.getInstance().getReference("territories").child(key).setValue(""+radius+"|"+MenuMapActivity.CURRENT_USER_ID);
                        return;
                    }
                }

                /*
                * If none exist create a new territory with 100 m radius
                * Also update the global list of territories
                * */
                double radius=100;
                String key=lat+"_"+lon;
                key=key.replaceAll("[.]","|");
                ter.put(key,""+radius);
                FirebaseDatabase.getInstance().getReference("users").child(MenuMapActivity.CURRENT_USER_ID).child("territories").child(key).setValue(""+radius);

                FirebaseDatabase.getInstance().getReference("territories").child(key).setValue(""+radius+"|"+MenuMapActivity.CURRENT_USER_ID);

                loadGame();
            }
        });

        // Change the add territory button action for multiplayer
        // -- compare scores of both players for wrong matches
        // -- increse the area of winner
        // -- decrease the area of looser
    }

    private void startMultiplayerGame() {
        currLabel=null;
        for(int j=0;j<4;j++){
            placeMultiplayerImage(images.get(j),gameData[mulTurn][j],labelData[mulTurn][j]);
        }
    }

    public boolean territoryOverlapped(LatLng hisCenter, LatLng myCenter, double hisRadius, double myRadius ){
        return calculateDistanceInKilometer(myCenter.latitude,myCenter.longitude,hisCenter.latitude,hisCenter.longitude)*1000 <
                (hisRadius+myRadius);
    }

    public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
    public double calculateDistanceInKilometer(double userLat, double userLng,
                                               double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return AVERAGE_RADIUS_OF_EARTH_KM * c;
    }

    private void loadGame() {
        currLabel= null;
        imageData.clear();
        Random rnd = new Random();
        String label = keys.get(rnd.nextInt(keys.size()));
//        Collections.shuffle(images);
        int countCorrect=2;
        String v= "";
        int n=4;
        for(int image: images) {

            if (n==countCorrect||(rnd.nextInt(2)==0)) {
                placeImage(image, rnd,label);
                countCorrect--;
            }else{
                placeImage(image, rnd,null);
            }
            v+=imageData.get(image)+" ";
            n--;
        }
    }

    private void initializeMul(){
        for(int i=0; i<20;i++){
            Random rnd = new Random();
            String label = keys.get(rnd.nextInt(keys.size()));
            int countCorrect=2;
            String v= "";
            int n=4;
            for(int image: images) {

                if (n==countCorrect||(rnd.nextInt(2)==0)) {
                    initializeBoard(i,4-n,label);
                    countCorrect--;
                }else{
                    initializeBoard(i,4-n,null);
                }
                n--;
            }
        }
    }

    private void initializeBoard(int gameNum,int imageNum, String label) {
        Random rnd = new Random();
        int data=rnd.nextInt(accData.size());
        if(label==null) label = keys.get(rnd.nextInt(keys.size()));
        int range = rnd.nextInt(annotations.get(label).size());
        int start=(int)annotations.get(label).get(range).getStart();
        int end =(int)annotations.get(label).get(range).getEnd();

        data = start + rnd.nextInt(end-start-200);
        gameData[gameNum][imageNum]=data;
        labelData[gameNum][imageNum]=label;
    }

    private void loadMultiplayerGame(String game, String label){
        String[] gameRow = game.split(":");
        String[] labelRow = label.split(":");
        for (int i=0; i<20;i++){
            String[] gameCol= gameRow[i].split(",");
            String[] labelCol= labelRow[i].split(",");
            for (int j=0; j<4; j++){
                gameData[i][j]= Integer.parseInt(gameCol[j]);
                labelData[i][j]= labelCol[j];
            }
        }

    }

    private void getMultiplayerBoard(String label) {

        // call place image (image, random, label) for each label
        for(int image: images) {
            currLabel= null;
            Random rnd = new Random();
            imageData.clear();
            int countCorrect=2;
            String v= "";
            int n=4;
            if (n==countCorrect||(rnd.nextInt(2)==0)) {
                placeImage(image, rnd,label);
                countCorrect--;
            }else{
                placeImage(image, rnd,null);
            }
            v+=imageData.get(image)+" ";
            n--;
        }
    }

    /*
    * Read the annotations file asynchronously such that annotations are ready before the game starts
    * */

    private void initializeAnnotations() {
        ArrayList<ActivityRange> al = new ArrayList<>();
        al.add(new ActivityRange(5149,5512));
        al.add(new ActivityRange(6634,7380));
        al.add(new ActivityRange(10330,10627));
        annotations.put("Shake the phone",al );
        al = new ArrayList<>();
        al.add(new ActivityRange(7720,8946));
        annotations.put("Jumping",al );
        al = new ArrayList<>();
        al.add(new ActivityRange(790,1519));
        annotations.put("Walking",al );
        al = new ArrayList<>();
        al.add(new ActivityRange(13201,14020));
        annotations.put("Stand up with phone in hand",al );
        al = new ArrayList<>();
        al.add(new ActivityRange(12550,13135));
        annotations.put("Hold the phone in hand and sit down",al );
    }

    public void placeImage(int image,Random rnd, String label){
        ImageView button = (ImageView) findViewById(image);
        int data=rnd.nextInt(accData.size());
        if(label==null) label = keys.get(rnd.nextInt(keys.size()));
        int range = rnd.nextInt(annotations.get(label).size());
        int start=(int)annotations.get(label).get(range).getStart();
        int end =(int)annotations.get(label).get(range).getEnd();

        data = start + rnd.nextInt(end-start-200);
        imageData.put(image,label);
        button.setImageDrawable(new CircleDrawable(accData,data));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundColor(getResources().getColor(R.color.green_color));
                String label= imageData.get(v.getId());
                v.setClickable(false);
                if(mode.equalsIgnoreCase("Multiplayer")){
                    processMultiplayerUserInput(label);
                }else {
                    processUserInput(label);
                }
//                showLabelToast(data);
            }
        });
    }

    public void placeMultiplayerImage(int image, int data, String label){
        ImageView button = (ImageView) findViewById(image);

        imageData.put(image,label);
        button.setImageDrawable(new CircleDrawable(accData,data));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundColor(getResources().getColor(R.color.green_color));
                String label= imageData.get(v.getId());
                v.setClickable(false);
                processMultiplayerUserInput(label);
//                showLabelToast(data);
            }
        });
    }

    private void processUserInput(String label) {
        if(currLabel==null) currLabel=label;
        else if(currLabel.equals(label)){
            progress+=5;
            ((ProgressBar)findViewById(R.id.territoryProgressBar)).setProgress(progress);
            if(progress>=100){
                findViewById(R.id.addTerritory).setVisibility(View.VISIBLE);
                findViewById(R.id.progressMessage).setVisibility(View.GONE);
            }else loadGame();
            getPreferences(MODE_PRIVATE).edit().putInt("progress",progress).commit();
        }else{
            loadGame();
        }
    }

    // Process multiplayer user input
    private void processMultiplayerUserInput(String label) {
        if(currLabel==null) {
            currLabel=label;
            return;
        }
        else if(currLabel.equals(label)){
            ((ProgressBar)findViewById(R.id.territoryProgressBar)).setProgress(mulTurn*5);
            getPreferences(MODE_PRIVATE).edit().putInt("progress",progress).commit();
            mulTurn++;
            startMultiplayerGame();
        }else{
            ((ProgressBar)findViewById(R.id.territoryProgressBar)).setProgress(mulTurn*5);
            mulTurn++;
            startMultiplayerGame();
            currentPlayerScore--;
        }

        if(mulTurn>=19){ //TODO: change 2 to 20
            if(playerTwo){
                FirebaseDatabase.getInstance().getReference(mulData).child(gameId).child("opponentScore").setValue(""+currentPlayerScore);
                if(currentPlayerScore>firstPlayerScore){
                    FirebaseDatabase.getInstance().getReference(mulData).child(gameId).child("winner").setValue(opponentPlayerId.replaceAll("[^a-zA-Z0-9]", "")+"|"+"opponent");
                }else{
                    FirebaseDatabase.getInstance().getReference(mulData).child(gameId).child("winner").setValue(challengerPlayerId.replaceAll("[^a-zA-Z0-9]", "")+"|"+"challenger");
                }
            }else {
                FirebaseDatabase.getInstance().getReference(mulData).child(gameId).child("challengerScore").setValue(""+currentPlayerScore);
            }

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You have done your best! \n Now fight for other territories");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
            builder.show();
            final String message = "Invite sent by";
            new Thread(new Runnable() {
                @Override
                public void run() {
                    pushNotification(message);
                }
            }).start();
        }
    }


    // Push notification
    private void pushNotification(String message) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Circle Territory");
            jNotification.put("body", "Your Territory has been challenged. Click here to save it");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "CircleActivity");

            jPayload.put("to" ,opponentToken);


            jPayload.put("priority", "high");

            JSONObject data = new JSONObject();
            data.put("gameId",gameId);
            jPayload.put("data", data);
            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            // Log.d(TAG, "Response from server - "+conn.getResponseCode());
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    //       Log.e(TAG, "run: " + resp);
                    Toast.makeText(getApplicationContext(),resp,Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    public void onresume (){
        connectionValueListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    System.out.println("connected");
                    AlertDialog.Builder builder = new AlertDialog.Builder(CircleActivity.this);
                    builder.setMessage("Trying to connect to Internet. Check if your internet is working correctly.Click OK if everything is working fine");
                    builder.setCancelable(false);
                    builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // nothing
                                }
                            });
                    try {AlertDialog mDialog = builder.show();}
                    catch (Exception e) {e.printStackTrace();}
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }};
        connectedRef.addValueEventListener(connectionValueListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            connectedRef.removeEventListener(connectionValueListener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}