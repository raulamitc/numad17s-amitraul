package edu.neu.madcourse.amitraul.scroggle;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import edu.neu.madcourse.amitraul.R;

public class ScroggleMainActivity extends AppCompatActivity {
    MediaPlayer mMediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroggle_main);
        if(getIntent().getExtras()!=null){
            Toast.makeText(this,getIntent().getExtras().get("email").toString(),Toast.LENGTH_SHORT).show();
            System.out.println(getIntent().getExtras().get("email").toString());
        }
//        Toast.makeText(this,savedInstanceState.get("email").toString(),Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = MediaPlayer.create(this, R.raw.scroggle_main_menu);
        mMediaPlayer.setVolume(0.5f, 0.5f);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
    }


}
