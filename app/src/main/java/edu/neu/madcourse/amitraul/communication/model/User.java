package edu.neu.madcourse.amitraul.communication.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by raula on 3/10/2017.
 */

public class User{
    private String email;
    private String name;
    private String score;
    private HashMap<String,String> friends= new HashMap<>();
    private String token;


    public User(String email, String name, String score, HashMap<String,String> friends, String token) {
        this.email = email;
        this.name = name;
        this.score = score;
        this.friends = friends;
        this.token= token;
    }

    public User() {
    }

    public User(String email, String name, String token) {
        this.email = email;
        this.name = name;
        this.token= token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public HashMap<String,String> getFriends() {
        return friends;
    }

    public void setFriends(HashMap<String,String> friends) {
        this.friends = friends;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
