package edu.neu.madcourse.amitraul.tricky;

/**
 * Created by raula on 4/6/2017.
 */

public class Accelerometer {
    float x;
    float y;
    float z;

    public Accelerometer(float v1, float v2, float v3) {
        x=v1; y=v2; z=v3;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
    public int getRGBHex(){
        int result=255<<8;

        result+= scaledNum(x);
        result=result<<8;
        result+=scaledNum(y);
        result=result<<8;
        result+=scaledNum(z);
//        result=result<<8;
        return result;
    }

    private int scaledNum(float n){
        int result=  (int)((n+12)*256/24);
        if (result<0) return 0;
        else if (result>255) return 255;
        else return result;

    }
}
