package edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.twoPlayer.ScroggleFriends;
import edu.neu.madcourse.amitraul.utility.Globals;

import static edu.neu.madcourse.amitraul.communication.SelectFriends.SERVER_KEY;


public class ScroggleGameActivity extends AppCompatActivity implements SensorListener {
    public static final String KEY_RESTORE = "key_restore";
    public static final String PREF_RESTORE = "pref_restore";
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private ScroggleGameFragment mGameFragment;
    protected static HashSet<Long> wordsLong;
    protected static HashSet<String> wordsStr;
    protected static HashSet<Integer> wordsInt;
    private static ArrayList<String> nineLetterWords;
    private static String[] patterns = {"036785214","036785241","214587630","254103678",
                                        "043678521","630124785","031467852","036784512",
                                        "036478512","401367852","425103678","748521036","037852146"};
    private TextView timerValue;
    private Long timeInMilliseconds=0L;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long updatedTime = 0L;
    protected static long timeSwapBuff = 0L;
    protected static boolean isNewGame= true;
    private boolean gamePaused=false;
    private boolean MUSIC_PAUSED=false;
    public DatabaseReference dataref;
    public Integer turns;
    private String first;
    private String second;
    private  String gameData;
    public String firstScore;
    public String secondScore;
    public String currentUser;
    public String otherUser;
    private OrientationEventListener mOrientationListener;
    private SensorManager sensorMgr;
    private DatabaseReference connectedRef;
    private ValueEventListener connectionValueListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDictionary();
        setContentView(R.layout.async_activity_scroggle_game);
        mGameFragment = (ScroggleGameFragment) getFragmentManager()
                .findFragmentById(R.id.fragment_game);
        dataref = FirebaseDatabase.getInstance().getReference().child("data");
        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);
        if (getIntent().getExtras().containsKey("turns")) {
            turns= Integer.valueOf((String) getIntent().getExtras().get("turns"));
            if(turns<=0) mGameFragment.displayScore();
            first= (String) getIntent().getExtras().get("first");
            second= (String) getIntent().getExtras().get("second");
            firstScore= (String) getIntent().getExtras().get(first);
            secondScore= (String) getIntent().getExtras().get(second);
            if(turns==9) restore=false;
            else restore =true;
            currentUser = first.equals(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""))?
                            first:second;
            otherUser = second.equals(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""))?
                        first:second;
            mGameFragment.score= Integer.parseInt((String) getIntent().getExtras().get(currentUser));
        }
        if (!restore) {
                timeSwapBuff=0L;
                gameData= initializeBoard();
                if (gameData != null) {
                    mGameFragment.putState(gameData);
                }
                dataref.child("game").child("async"+first+second).child("gameData").setValue(gameData);
                timerValue = (TextView) findViewById(R.id.timerValue);
                startTimer(ScroggleGameFragment.MAX_TIME);
            }else{
                timerValue = (TextView) findViewById(R.id.timerValue);
                dataref.child("game").child("async"+first+second).child("gameData").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
             /*           for(DataSnapshot ds: dataSnapshot.getChildren()){
                            if(ds.getKey().equals("gameData")){
                                gameData= (String) ds.getValue();
                                mGameFragment.putState(gameData);
                            }

                        }*/
                        gameData= (String) dataSnapshot.getValue();
                        if (gameData != null) {
                             mGameFragment.putState(gameData);
                        }
                        startTimer(ScroggleGameFragment.MAX_TIME);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }


/*        Log.d("UT3", "restore = " + restore);
        if(isNewGame){
            startTimer(ScroggleGameFragment.MAX_TIME);
        }else{
            startTimer(Globals.currentTime);
        }*/

        Button btn1= (Button)findViewById(R.id.done_button);
        Button clearBtn = (Button)findViewById(R.id.clear_button);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.current_word);
                tv.setText("");
                mGameFragment.failedWord();
            }});
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.current_word);
                String word= tv.getText().toString();
                tv.setText("");
                int l= word.length();
                if (l <= 6) {
                    int queryInt = 0;
                    for (char c : word.toCharArray()) {
                        queryInt += c - 96;
                        queryInt = queryInt << 5;
                    }
                    if (ScroggleGameActivity.wordsInt.contains(queryInt)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }

                } else if (l <= 12&& l!=9) {
                    long queryLong = 0;
                    for (char c : word.toCharArray()) {
                        queryLong += c - 96;
                        queryLong = queryLong << 5;
                    }
                    if (ScroggleGameActivity.wordsLong.contains(queryLong)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }
                }else{
                    if (ScroggleGameActivity.wordsStr.contains(word)) {
                        mGameFragment.wordSuccess();
                    }else{
                        mGameFragment.failedWord();
                    }
                }
                Log.d("scrog-Game-frag","button clicked");
            }
        });

        sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorMgr.registerListener(this,
                SensorManager.SENSOR_ACCELEROMETER,
                10000000);
    }


    private boolean turnedOff=false;
    Runnable updateTimerThread = new Runnable() {

             public void run() {

                    timerValue.setText(String.format("%02d", Globals.currentTime));
                    if(turns==0) {
                        mGameFragment.wordSuccess();
                        finishTurn();
                    }
                    if (Globals.currentTime == 0) {
                        mGameFragment.wordSuccess();
                        finishTurn();
                    }
                    if (Globals.currentTime <= 4) {
                        timerValue.setTextColor(getResources().getColor(R.color.red_color));
                    } else {
                        timerValue.setTextColor(getResources().getColor(R.color.white_color));
                    }
                    Globals.currentTime--;
                    customHandler.postDelayed(this, 1000);

             }

        };

    public void beep() {
        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
    }


    protected void loadDictionary() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    wordsInt = Globals.getWordsInt(getResources().openRawResource(R.raw.wordlist_en_int));
                    wordsLong = Globals.getWordsLong(getResources().openRawResource(R.raw.wordlist_en_long8));
                    wordsStr = Globals.getWords(getResources().openRawResource(R.raw.wordlist_en_str9));
                    nineLetterWords=Globals.getNineLetterWords();
//                    Snackbar.make(findViewById(android.R.id.content),
//                            "dictionary loaded",
//                            Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Snackbar.make(findViewById(android.R.id.content),
                            "restart the app please!",
                            Snackbar.LENGTH_SHORT).show();
                }
            }
        }).start();
    }

    private String initializeBoard() {
        StringBuilder sb = new StringBuilder();
        while(nineLetterWords==null){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            nineLetterWords=Globals.getNineLetterWords();
        }
        int l =nineLetterWords.size();
        sb.append("-1,-1,");
        Random rand = new Random();
        for(int i=0;i<9;i++){
            String word=nineLetterWords.get(rand.nextInt(l));
            char[] mat= getScrambledMatrix(word,rand);
            for(char c: mat){
                sb.append("NEITHER,");
                sb.append(c);
                sb.append(",");
            }
            System.out.println(word);
        }
       return  sb.toString();
    }

    protected char[] getScrambledMatrix(String word, Random rand) {
        char[] wordArr= new char[9];

        String pattern= patterns[rand.nextInt(patterns.length)];
        char[] charArray= pattern.toCharArray();
        int flag = rand.nextInt(2);
        for(int i=0; i<9;i++){
            if(flag==1) wordArr[charArray[i]-'0']=word.charAt(i);
            else wordArr[8-(charArray[i]-'0')]=word.charAt(i);
        }
        return wordArr;
    }

    public void restartGame() {
        mGameFragment.restartGame();
    }

    public void reportWinner(final ScroggleTile.Owner winner) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
        builder.setMessage(getString(R.string.declare_winner, winner));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok_label,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        final Dialog dialog = builder.create();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMediaPlayer = MediaPlayer.create(ScroggleGameActivity.this,
                        winner == ScroggleTile.Owner.X ? R.raw.oldedgar_winner
                                : winner == ScroggleTile.Owner.O ? R.raw.notr_loser
                                : R.raw.department64_draw
                );
                mMediaPlayer.start();
                dialog.show();
            }
        }, 500);

        // Reset the board to the initial position
        mGameFragment.initGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = MediaPlayer.create(this, R.raw.scroggle_game_play);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
        if (gamePaused) pauseGame();

        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectionValueListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    System.out.println("connected");
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScroggleGameActivity.this);
                    builder.setMessage("Trying to connect to Internet. Check if your internet is working correctly.Click OK if everything is working fine");
                    builder.setCancelable(false);
                    builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // nothing
                                }
                            });
                    try {AlertDialog mDialog = builder.show();}
                    catch (Exception e) {e.printStackTrace();}
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }};

        connectedRef.addValueEventListener(connectionValueListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseTimer();
        gamePaused=true;
        mHandler.removeCallbacks(null);
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        String gameData = mGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .commit();
        Log.d("UT3", "state = " + gameData);
        connectedRef.removeEventListener(connectionValueListener);

    }


    public void startTimer(int time) {
        Globals.currentTime=time;
        pauseTimer();
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }
    public void pauseTimer(){
        //timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
    }

    public void pauseMusic() {
        if(mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            MUSIC_PAUSED=true;
        }
        else {
            mMediaPlayer.start();
            MUSIC_PAUSED=false;
        }
    }

    public void pauseGame() {
        pauseTimer();
        if(MUSIC_PAUSED && mMediaPlayer.isPlaying())  mMediaPlayer.pause();
        pauseMusic();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setPositiveButton("Resume",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startTimer(Globals.currentTime);
                        if(MUSIC_PAUSED && mMediaPlayer.isPlaying()) mMediaPlayer.start();;
                    }
                });
        builder.show();
    }

    public void finishTurn(){
        if(getIntent().getExtras().containsKey("turns")){
//            turns--;
            if(turns<=0){
                mGameFragment.displayScore();
                FirebaseDatabase.getInstance().getReference("data").child("users").child(otherUser).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
                        pushNotificationNewThread(user.getToken());
                        finish();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                Toast.makeText(this,"No more Turns Left! Game Over!",Toast.LENGTH_SHORT).show();
            }else{
                dataref.child("game").child("async"+first+second).child("someGameTurn").setValue(turns);
                dataref.child("game").child("async"+first+second).child("currentUser").setValue(otherUser);
                Toast.makeText(this,"Total Turns Left: "+turns,Toast.LENGTH_SHORT).show();

                FirebaseDatabase.getInstance().getReference("data").child("users").child(otherUser).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
                        pushNotificationNewThread(user.getToken());
                        dataref.child("game").child("async"+first+second).child("gameData").setValue(mGameFragment.getState());
                        finish();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }
        }else{
            if(turns<0){
                Toast.makeText(this,"No more Turns Left",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Not your turn",Toast.LENGTH_SHORT).show();
            }
            finish();
        }


    }
    public void pushNotificationNewThread(final String clientToken) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification(clientToken);
            }
        }).start();
    }
    private void pushNotification(String clientToken) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Scroggle");
            jNotification.put("body", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has invited you to play scroggle");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "AsyncScroggle");

            // If sending to a single client
            jPayload.put("to", clientToken);
            JSONObject data = new JSONObject();
            try {
                data.put("first",getIntent().getExtras().get("first"));
                data.put("second",getIntent().getExtras().get("second"));
                data.put(currentUser,mGameFragment.score);
                data.put(otherUser,getIntent().getExtras().get(otherUser));
                data.put("game_id", (String) getIntent().getExtras().get("first") + getIntent().getExtras().get("second"));
                data.put("turns",turns);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

            jPayload.put("data",data);

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("in friend list adapter", "run: " + resp);
                    Toast.makeText(getApplicationContext(),resp,Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    @Override
    public void onSensorChanged(int sensor, float[] values) {
        if (sensor == SensorManager.SENSOR_ACCELEROMETER) {


                float x = values[SensorManager.DATA_X];
                float y = values[SensorManager.DATA_Y];
                float z = values[SensorManager.DATA_Z];

                if (y>4) {
                    mGameFragment.rotateAllTiles(180);
//                    System.out.println("270 degree");
                }else if (x<-4) mGameFragment.rotateAllTiles(90); //System.out.println("90 degre");
                else if (y<-4) mGameFragment.rotateAllTiles(0); //System.out.println("0 degree");
                else mGameFragment.rotateAllTiles(270);//System.out.println("180 degree");
//                System.out.println("x: "+x +"\ty: "+ y+"\tz: "+z);

            }
    }

    @Override
    public void onAccuracyChanged(int sensor, int accuracy) {

    }

}
