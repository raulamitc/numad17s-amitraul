package edu.neu.madcourse.amitraul.finalApp.model;

/**
 * Created by raula on 4/20/2017.
 */

public class ActivityRange {
    private long start;
    private long end;

    public ActivityRange() {
    }

    public ActivityRange(long start, long end) {
        this.start = start;
        this.end = end;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }
}
