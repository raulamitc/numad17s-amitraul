package edu.neu.madcourse.amitraul.communication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.utility.Globals;

public class OnlineFriendsPlayTest extends AppCompatActivity {

    private DatabaseReference dataref;
    private TextView tvGameData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_friends_play_test);

        if(getIntent().getExtras().containsKey("token")){
            dataref= FirebaseDatabase.getInstance().getReference().child("data");
            dataref.child("game").child("someGame").setValue(initializeBoard());
        }
        tvGameData= (TextView) findViewById(R.id.gamedataTV);
        dataref.child("game").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("someGame")) {
                    tvGameData.setText(dataSnapshot.getValue(String.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                tvGameData.setText(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private String initializeBoard() {
        StringBuilder sb = new StringBuilder();

        sb.append("-1,-1,0,");
        Random rand = new Random();
        for(int i=0;i<9;i++){
            char[] mat= getScrambledMatrix(rand);
            for(char c: mat){
                sb.append("NEITHER,");
                sb.append(c);
                sb.append(",");
            }
        }
        return  sb.toString();
    }

    protected char[] getScrambledMatrix(Random rand) {
        char[] wordArr= new char[9];

        int flag = rand.nextInt(2);
        for(int i=0; i<9;i++){
            wordArr[i]= (char) ('A'+ rand.nextInt(26));
        }
        return wordArr;
    }

    public void refreshGame(View view){
        initializeBoard();
        dataref.child("game").child("someGame").setValue(initializeBoard());
    }
}
