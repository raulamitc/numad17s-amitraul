package edu.neu.madcourse.amitraul.finalApp.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.model.Territory;
import edu.neu.madcourse.amitraul.finalApp.model.User;

/**
 * Created by raula on 4/28/2017.
 */

public class TerritoryAdapter extends RecyclerView.Adapter<TerritoryAdapter.ViewHolder> {
private ArrayList<Territory> mDataset= new ArrayList<>();

public class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    public TextView txtHeader;
    public TextView txtFooter;

    public ViewHolder(View v) {
        super(v);
        txtHeader = (TextView) v.findViewById(R.id.location);
        txtFooter = (TextView) v.findViewById(R.id.radius);
    }
}

    public void add(int position, Territory item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TerritoryAdapter(User user) {
        for (String key:user.getMyTerritories().keySet()) {
            Territory ter = new Territory();
            ter.setLocation(key);
            ter.setRadius(String.format("%.2f",Double.parseDouble(user.getMyTerritories().get(key)))+" meters");
            mDataset.add(ter);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TerritoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.territory_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        TerritoryAdapter.ViewHolder vh = new TerritoryAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(TerritoryAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Territory ter = mDataset.get(position);
        holder.txtHeader.setText(ter.getLocation());
        holder.txtFooter.setText(ter.getRadius());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}