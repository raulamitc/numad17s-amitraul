package edu.neu.madcourse.amitraul.testDict;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.HashSet;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.utility.Globals;

public class TestDictActivity extends AppCompatActivity {

    private static HashSet<String> userList;
    private static HashSet<String> wordsStr;
    private static HashSet<Integer> wordsInt;
    private static HashSet<Long> wordsLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("dict loaded");
        setContentView(R.layout.activity_test_dict);
        final ProgressDialog pd = ProgressDialog.show(this, "Loading", "Dictionary loading...");

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lockScreenOrientation();
                    wordsInt = Globals.getWordsInt(getResources().openRawResource(R.raw.wordlist_en_int));
                    wordsLong = Globals.getWordsLong(getResources().openRawResource(R.raw.wordlist_en_long));
                    wordsStr = Globals.getWords(getResources().openRawResource(R.raw.wordlist_en_str));
                    Snackbar.make(findViewById(android.R.id.content),
                            "dictionary loaded",
                            Snackbar.LENGTH_SHORT).show();
                    unlockScreenOrientation();
                } catch (IOException e) {
                    Snackbar.make(findViewById(android.R.id.content),
                            "restart the app please!",
                            Snackbar.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }
        }).start();

        final EditText dictWord = (EditText) findViewById(R.id.dictWord);
        final TextView list = (TextView) findViewById(R.id.dict_list);
        dictWord.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                String query = cs.toString();
                if(userList==null){
                    userList=new HashSet<String>();
                }
                if (query.length() <= 6) {
                    int queryInt = 0;
                    for (char c : query.toCharArray()) {
                        queryInt += c - 96;
                        queryInt = queryInt << 5;
                    }
                    if (wordsInt.contains(queryInt)) {
                        beep();
                        userList.add(query);
                        String listOfWords = "";
                        for (String word : userList) {
                            listOfWords += word + " ";
                        }
                        list.setText(listOfWords);
                    }

                } else if (query.length() <= 12) {
                    long queryLong = 0;
                    for (char c : query.toCharArray()) {
                        queryLong += c - 96;
                        queryLong = queryLong << 5;
                    }
                    if (wordsLong.contains(queryLong)) {
                        beep();
                        userList.add(query);
                        String listOfWords = "";
                        for (String word : userList) {
                            listOfWords += word + " ";
                        }
                        list.setText(listOfWords);
                    }
                } else {
                    if (wordsStr.contains(cs.toString())) {
                        beep();
                        userList.add(cs.toString());
                        String listOfWords = "";
                        for (String word : userList) {
                            listOfWords += word + " ";
                        }
                        list.setText(listOfWords);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void clearText(View v) {
        EditText dictWord = (EditText) findViewById(R.id.dictWord);
        TextView list = (TextView) findViewById(R.id.dict_list);
        this.userList.clear();
        dictWord.setText("");
        list.setText("");
    }

    public void returnToMenu(View view) {
        finish();
    }

    public void getAck(View view) {
        Intent intent = new Intent(this, DictAcknowledgement.class);
        startActivity(intent);
    }

    public void beep() {
        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
    }

    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

}
