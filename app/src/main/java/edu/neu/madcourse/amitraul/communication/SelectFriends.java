package edu.neu.madcourse.amitraul.communication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.communication.util.FriendListAdapter;

public class SelectFriends extends AppCompatActivity {

    private static final String TAG = "Select friends ";
    public static final String SERVER_KEY = "key=AAAAd7PmPW4:APA91bGBLeoJFAcLQHh4utfL6BzpIX9kgneWcXL7ACwP_ATKCy5SZnUemsQooeiQisTBXxPsmK2k0WY0avFgw_luOwI0oh16PIGf-JLdxBhfTy9qpl89HahGDhr8zR3Pvkq_UkRIAv51";
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_friends);


        recyclerView = (RecyclerView) findViewById(R.id.off_friends_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    System.out.println("connected");
                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectFriends.this);
                    builder.setMessage("Trying to connect to Internet. Check if your internet is working correctly.Click OK if everything is working fine");
                    builder.setCancelable(false);
                    builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // nothing
                                }
                            });
                    try {AlertDialog mDialog = builder.show();}
                    catch (Exception e) {e.printStackTrace();}
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }});
            // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        final String key=FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]","");
        mDatabase = FirebaseDatabase.getInstance().getReference("data");
        final DatabaseReference currentUserRef = mDatabase.child("users").child(key);

        currentUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user=dataSnapshot.getValue(User.class);
                FriendListAdapter mAdapter = new FriendListAdapter(new ArrayList<String>(user.getFriends().values()));
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        // specify an adapter (see also next example)


        Button addNewFriendsBtn= (Button) findViewById(R.id.addNewFriends);
        addNewFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                LinearLayout addFriendLayout = (LinearLayout) findViewById(R.id.addFriendLayout);
                addFriendLayout.setVisibility(View.VISIBLE);
            }
        });

        Button addFriendBtn= (Button) findViewById(R.id.addFriend);
        addFriendBtn.setOnClickListener(new View.OnClickListener() {
            public TextView emailText;

            @Override
            public void onClick(View view) {
//                view.setVisibility(View.GONE);
                emailText= (TextView) findViewById(R.id.friendId);
                final String emailId = emailText.getText().toString();
                final String friendKey=emailId.replaceAll("[^a-zA-Z0-9]","");
                if(!TextUtils.isEmpty(emailId) && Patterns.EMAIL_ADDRESS.matcher(emailId).matches()){
                    mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            HashMap<String,HashMap> userMap =(HashMap<String,HashMap>)snapshot.getValue();
                            if(userMap.containsKey(friendKey)){
                                if(userMap.get(friendKey).get("friends")==null){
                                    userMap.get(friendKey).put("friends",new HashMap<String,String>());
                                }
                                ((HashMap)userMap.get(friendKey).get("friends")).put(key,key);
                                if(userMap.get(key).get("friends")==null){
                                    userMap.get(key).put("friends",new HashMap<String,String>());
                                }
                                ((HashMap)userMap.get(key).get("friends")).put(friendKey,friendKey);
                                mDatabase.child("users").setValue(userMap);
//                                pushNotificationNewThread((String)userMap.get(friendKey).get("token"));
                                Toast.makeText(getApplicationContext(),"Friend Added!", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(getApplicationContext(),"Invite your friend", Toast.LENGTH_SHORT).show();
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
                }else{
                    emailText.setError("Shucks! you've entered an incorrect email address");
                }
                LinearLayout addFriendLayout = (LinearLayout) findViewById(R.id.addFriendLayout);
                addFriendLayout.setVisibility(View.VISIBLE);
            }
        });


        Button onlineFriendsPlayBtn= (Button) findViewById(R.id.onlineFriendPlayBtn);
        onlineFriendsPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView emailOnline= (TextView) findViewById(R.id.onlineFriendID);
                final Intent intent = new Intent(getApplicationContext(),OnlineFriendsPlayTest.class);
                mDatabase.child("users").child(emailOnline.getText().toString().replaceAll("[^a-zA-Z0-9]",""))
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        User user = dataSnapshot.getValue(User.class);
                        intent.putExtra("token", user.getToken());
                        startActivity(intent);
                    }catch (Exception e){
                        System.out.println("error at online test" );
                        intent.putExtra("token", "someRandomtoken");
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    public void pushNotificationNewThread(final String clientToken) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification(clientToken);
            }
        }).start();
    }
    private void pushNotification(String clientToken) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Google I/O 2016");
            jNotification.put("body", "Firebase Cloud Messaging (App)");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "AsyncGameActivity");

            // If sending to a single client
            jPayload.put("to", clientToken);

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "run: " + resp);
                    Toast.makeText(getApplicationContext(),resp,Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

}
