package edu.neu.madcourse.amitraul.twoPlayer.util;

/**
 * Created by raula on 3/11/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.OfflineGameTestActivity;
import edu.neu.madcourse.amitraul.communication.model.User;
import edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle.ScroggleGameActivity;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {
//    private  TextView txtHeader;
    private ArrayList<String> mDataset;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) v.findViewById(R.id.firstLine);
        }
    }

    public void add(int position, String item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FriendListAdapter(ArrayList<String> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FriendListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list, parent, false);
        context=parent.getContext();
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final String name = mDataset.get(position);
        holder.txtHeader.setText(mDataset.get(position));
        holder.txtHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                DatabaseReference mdataref = FirebaseDatabase.getInstance().getReference("data").child("users").child(holder.txtHeader.getText().toString());
                mdataref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
//                        pushNotificationNewThread(user.getToken());
                        Intent intent = new Intent(v.getContext(),ScroggleGameActivity.class);
                        intent.putExtra("turns","9");
                        intent.putExtra("first",FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""));
                        intent.putExtra("second",holder.txtHeader.getText().toString());
                        intent.putExtra(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""),"0");
                        intent.putExtra(holder.txtHeader.getText().toString(),"0");

                        FirebaseDatabase.getInstance().getReference("data").child("game")
                                .child(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]","")
                                +holder.txtHeader.getText()).child("current_player").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail());
                        context.startActivity(intent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                final DatabaseReference dataref = FirebaseDatabase.getInstance().getReference().child("data");
                dataref.child("game").child("currentUser").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""));
//                remove(name);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}