package edu.neu.madcourse.amitraul.scroggle;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import edu.neu.madcourse.amitraul.ticTac.GameFragment;
import edu.neu.madcourse.amitraul.ticTac.Tile;

/**
 * Created by raula on 2/15/2017.
 */

public class ScroggleTile {


    public enum Owner {
        X, O , NEITHER, BOTH, Z
    }


    // These levels are defined in the drawable definitions
    private static final int LEVEL_X = 0;
    private static final int LEVEL_O = 1; // letter O
    private static final int LEVEL_BLANK = 2;
    private static final int LEVEL_AVAILABLE = 3;
    private static final int LEVEL_TIE = 3;
    private static final int LEVEL_Z=4;
    public String text;

    private final ScroggleGameFragment mGame;
    private ScroggleTile.Owner mOwner = ScroggleTile.Owner.NEITHER;
    private View mView;
    private ScroggleTile mSubTiles[];

    public ScroggleTile(ScroggleGameFragment game) {
        this.mGame = game;
    }

    public ScroggleTile deepCopy() {
        ScroggleTile tile = new ScroggleTile(mGame);
        tile.setOwner(getOwner());
        if (getSubTiles() != null) {
            ScroggleTile newTiles[] = new ScroggleTile[9];
            ScroggleTile oldTiles[] = getSubTiles();
            for (int child = 0; child < 9; child++) {
                newTiles[child] = oldTiles[child].deepCopy();
            }
            tile.setSubTiles(newTiles);
        }
        return tile;
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public ScroggleTile.Owner getOwner() {
        return mOwner;
    }

    public void setOwner(ScroggleTile.Owner owner) {
        this.mOwner = owner;
    }

    public ScroggleTile[] getSubTiles() {
        return mSubTiles;
    }

    public void setSubTiles(ScroggleTile[] subTiles) {
        this.mSubTiles = subTiles;
    }

    public void updateDrawableState() {
        if (mView == null) return;
        int level = getLevel();
        if (mView.getBackground() != null) {
            mView.getBackground().setLevel(level);
        }
        if (mView instanceof Button) {
            ((Button) mView).setText(text);
//            drawable.setLevel(1);
        }
    }

/*    private char getChar(){

    }*/
    private int getLevel() {
        int level = LEVEL_BLANK;
        switch (mOwner) {
            case X:
                level = LEVEL_X;
                break;
            case O: // letter O
                level = LEVEL_O;
                break;
            case BOTH:
                level = LEVEL_TIE;
                break;
            case Z:
                level= LEVEL_Z;
                break;
            case NEITHER:
                level = mGame.isAvailable(this) ? LEVEL_AVAILABLE : LEVEL_BLANK;
                break;
        }
        return level;
    }

    private void countCaptures(int totalX[], int totalO[]) {
        int capturedX, capturedO;
        // Check the horizontal
        for (int row = 0; row < 3; row++) {
            capturedX = capturedO = 0;
            for (int col = 0; col < 3; col++) {
                ScroggleTile.Owner owner = mSubTiles[3 * row + col].getOwner();
                if (owner == ScroggleTile.Owner.X || owner == ScroggleTile.Owner.BOTH) capturedX++;
                if (owner == ScroggleTile.Owner.O || owner == ScroggleTile.Owner.BOTH) capturedO++;
            }
            totalX[capturedX]++;
            totalO[capturedO]++;
        }

        // Check the vertical
        for (int col = 0; col < 3; col++) {
            capturedX = capturedO = 0;
            for (int row = 0; row < 3; row++) {
                ScroggleTile.Owner owner = mSubTiles[3 * row + col].getOwner();
                if (owner == ScroggleTile.Owner.X || owner == ScroggleTile.Owner.BOTH) capturedX++;
                if (owner == ScroggleTile.Owner.O || owner == ScroggleTile.Owner.BOTH) capturedO++;
            }
            totalX[capturedX]++;
            totalO[capturedO]++;
        }

        // Check the diagonals
        capturedX = capturedO = 0;
        for (int diag = 0; diag < 3; diag++) {
            ScroggleTile.Owner owner = mSubTiles[3 * diag + diag].getOwner();
            if (owner == ScroggleTile.Owner.X || owner == ScroggleTile.Owner.BOTH) capturedX++;
            if (owner == ScroggleTile.Owner.O || owner == ScroggleTile.Owner.BOTH) capturedO++;
        }
        totalX[capturedX]++;
        totalO[capturedO]++;
        capturedX = capturedO = 0;
        for (int diag = 0; diag < 3; diag++) {
            ScroggleTile.Owner owner = mSubTiles[3 * diag + (2 - diag)].getOwner();
            if (owner == ScroggleTile.Owner.X || owner == ScroggleTile.Owner.BOTH) capturedX++;
            if (owner == ScroggleTile.Owner.O || owner == ScroggleTile.Owner.BOTH) capturedO++;
        }
        totalX[capturedX]++;
        totalO[capturedO]++;
    }

    public ScroggleTile.Owner findWinner() {
        // If owner already calculated, return it
        if (getOwner() != ScroggleTile.Owner.NEITHER)
            return getOwner();

        int totalX[] = new int[4];
        int totalO[] = new int[4];
        countCaptures(totalX, totalO);
        if (totalX[3] > 0) return ScroggleTile.Owner.X;
        if (totalO[3] > 0) return ScroggleTile.Owner.O;

        // Check for a draw
        int total = 0;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                ScroggleTile.Owner owner = mSubTiles[3 * row + col].getOwner();
                if (owner != ScroggleTile.Owner.NEITHER) total++;
            }
            if (total == 9) return ScroggleTile.Owner.BOTH;
        }

        // Neither player has won this ScroggleTile
        return ScroggleTile.Owner.NEITHER;
    }

    public int evaluate() {
        switch (getOwner()) {
            case X:
                return 100;
            case O:
                return -100;
            case NEITHER:
                int total = 0;
                if (getSubTiles() != null) {
                    for (int tile = 0; tile < 9; tile++) {
                        total += getSubTiles()[tile].evaluate();
                    }
                    int totalX[] = new int[4];
                    int totalO[] = new int[4];
                    countCaptures(totalX, totalO);
                    total = total * 100 + totalX[1] + 2 * totalX[2] + 8 *
                            totalX[3] - totalO[1] - 2 * totalO[2] - 8 * totalO[3];
                }
                return total;
        }
        return 0;
    }

    public void animate() {
        Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
                edu.neu.madcourse.amitraul.R.animator.tictactoe);
        if (getView() != null) {
            anim.setTarget(getView());
            anim.start();
        }
    }
}
