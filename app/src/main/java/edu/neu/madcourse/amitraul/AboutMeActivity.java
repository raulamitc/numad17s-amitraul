package edu.neu.madcourse.amitraul;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.TextView;

public class AboutMeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.neu.madcourse.amitraul.R.layout.activity_about_me);
        TextView imeiText = (TextView) findViewById(edu.neu.madcourse.amitraul.R.id.imeiTextView);
        TelephonyManager telephonyManager =  (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        imeiText.setText(telephonyManager.getDeviceId());
    }
}
