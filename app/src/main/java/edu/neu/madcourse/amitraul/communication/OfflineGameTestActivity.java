package edu.neu.madcourse.amitraul.communication;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.Scanner;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.communication.model.User;

import static edu.neu.madcourse.amitraul.communication.SelectFriends.SERVER_KEY;

public class OfflineGameTestActivity extends AppCompatActivity {

    private DatabaseReference dataref;
    private TextView tvGameData;
    private int turns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_game_test);
        dataref = FirebaseDatabase.getInstance().getReference().child("data");
        if (getIntent().getExtras().containsKey("turns")) {
            turns= Integer.valueOf((String) getIntent().getExtras().get("turns"));
            if(turns==10) dataref.child("game").child("someGame").setValue(initializeBoard());
        }
        if(turns<0){
            Toast.makeText(this,"No more Turns Left! Game Over!",Toast.LENGTH_SHORT).show();
        }
        tvGameData = (TextView) findViewById(R.id.offGameDataTv);
        dataref.child("game").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("someGame")) {
                    tvGameData.setText(dataSnapshot.getValue(String.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("someGame")) {
                    tvGameData.setText(dataSnapshot.getValue(String.class));
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    private String initializeBoard() {
        StringBuilder sb = new StringBuilder();

        sb.append("-1,-1,0,");
        Random rand = new Random();
        for (int i = 0; i < 9; i++) {
            char[] mat = getScrambledMatrix(rand);
            for (char c : mat) {
                sb.append("NEITHER,");
                sb.append(c);
                sb.append(",");
            }
        }
        return sb.toString();
    }

    protected char[] getScrambledMatrix(Random rand) {
        char[] wordArr = new char[9];

        int flag = rand.nextInt(2);
        for (int i = 0; i < 9; i++) {
            wordArr[i] = (char) ('A' + rand.nextInt(26));
        }
        return wordArr;
    }

    public void playTurn(View view) {
//        initializeBoard();

        dataref.child("game").child("currentUser").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null||dataSnapshot.getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""))){
                    dataref.child("game").child("someGame").setValue(initializeBoard());
                }else{
                    Toast.makeText(getApplicationContext(),"Sorry Not your turn",Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        dataref.child("game").child("currentUser").removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void finishTurn(View view){
//        initializeBoard();
//        dataref.child("game").child("someGame").setValue(initializeBoard());
        if(getIntent().getExtras().containsKey("turns")){
            turns--;
            if(turns<0){
                Toast.makeText(this,"No more Turns Left! Game Over!",Toast.LENGTH_SHORT).show();
            }else{
                dataref.child("game").child("someGameTurn").setValue(turns);
                String currentUser = (String) (getIntent().getExtras().get("first").equals(FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[^a-zA-Z0-9]",""))?
                                        getIntent().getExtras().get("second"):getIntent().getExtras().get("first"));
                dataref.child("game").child("currentUser").setValue(currentUser);
                Toast.makeText(this,"Total Turns Left: "+turns,Toast.LENGTH_SHORT).show();

                FirebaseDatabase.getInstance().getReference("data").child("users").child(currentUser).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
                        pushNotificationNewThread(user.getToken());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        }else{
            if(turns<0){
                Toast.makeText(this,"No more Turns Left",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Not your turn",Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }


    public void pushNotificationNewThread(final String clientToken) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification(clientToken);
            }
        }).start();
    }
    private void pushNotification(String clientToken) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Scroggle");
            jNotification.put("body", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has invited you to play scroggle");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "AsyncScroggle");

            // If sending to a single client
            jPayload.put("to", clientToken);
            JSONObject data = new JSONObject();
            try {
                data.put("first",getIntent().getExtras().get("first"));
                data.put("second",getIntent().getExtras().get("second"));
                data.put("game_id", (String)getIntent().getExtras().get("first") + getIntent().getExtras().get("second"));
                data.put("turns",turns);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }

            jPayload.put("data",data);

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("in friend list adapter", "run: " + resp);
                    Toast.makeText(getApplicationContext(),resp,Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

}