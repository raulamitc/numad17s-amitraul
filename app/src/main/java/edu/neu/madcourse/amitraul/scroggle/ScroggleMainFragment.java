package edu.neu.madcourse.amitraul.scroggle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.ticTac.GameActivity;
import edu.neu.madcourse.amitraul.utility.Globals;

public class ScroggleMainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private android.app.AlertDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.fragment_scroggle_main, container, false);
        try {
            Globals.getWords(getResources().openRawResource(R.raw.wordlist_en_str9));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Handle buttons here...
        View newButton = rootView.findViewById(edu.neu.madcourse.amitraul.R.id.new_button);
        View continueButton = rootView.findViewById(edu.neu.madcourse.amitraul.R.id.continue_button);
        View aboutButton = rootView.findViewById(edu.neu.madcourse.amitraul.R.id.about_button);
        View ackButton= rootView.findViewById(R.id.ack_button);
        View quitButton = rootView.findViewById(R.id.quit_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScroggleGameActivity.class);
                getActivity().startActivity(intent);
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScroggleGameActivity.class);
                intent.putExtra(GameActivity.KEY_RESTORE, true);
                getActivity().startActivity(intent);
            }
        });
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.about_scroggle);
                builder.setCancelable(false);
                builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });
        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                Spanned message= Html.fromHtml(getResources().getString(R.string.scroggleHtmlAck));
                builder.setMessage(message);
                builder.setCancelable(false);
                builder.setPositiveButton(edu.neu.madcourse.amitraul.R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

//        loadDictionary();
        return rootView;

    }

/*
    protected void loadDictionary() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        "Loading the game, please wait...",
                        Snackbar.LENGTH_SHORT).show();
                try {
                    Globals.getWordsInt(getResources().openRawResource(R.raw.wordlist_en_int));
                    Snackbar.make(getActivity().findViewById(android.R.id.content),
                            "Game Loaded.",
                            Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content),
                            "restart the app please!",
                            Snackbar.LENGTH_SHORT).show();
                }
            }
        }).start();
    }
*/




    @Override
    public void onPause() {
        super.onPause();

        // Get rid of the about dialog if it's still up
        if (mDialog != null)
            mDialog.dismiss();
    }
}
