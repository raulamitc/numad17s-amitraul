package edu.neu.madcourse.amitraul.finalApp.model;

import java.util.HashMap;

/**
 * Created by raula on 4/20/2017.
 */

public class Territories {
    HashMap<String,String> territoryOwners = new HashMap<>();

    public HashMap<String, String> getTerritoryOwners() {
        return territoryOwners;
    }
    public void setTerritoryOwners(HashMap<String, String> territoryOwners) {
        this.territoryOwners = territoryOwners;
    }

}
