package edu.neu.madcourse.amitraul.twoPlayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

import edu.neu.madcourse.amitraul.twoPlayer.syncScroggle.ScroggleGameActivity;


/**
 * Created by raula on 3/29/2017.
 */

public class ConnectivityBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(ConnectivityManager.CONNECTIVITY_ACTION)){
            if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,false)){
//                if(context instanceof ScroggleGameActivity || context instanceof edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle.ScroggleGameActivity){
                    Toast.makeText(context,"Internet Connection lost!!",Toast.LENGTH_SHORT);
//                }
            }else{
//                if(context instanceof ScroggleGameActivity || context instanceof edu.neu.madcourse.amitraul.twoPlayer.asyncScroggle.ScroggleGameActivity){
                    Toast.makeText(context,"Internet Connection Found!!",Toast.LENGTH_SHORT);
//                }
            }
        }

    }
}
