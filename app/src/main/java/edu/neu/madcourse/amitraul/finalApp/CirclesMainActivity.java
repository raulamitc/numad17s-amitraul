package edu.neu.madcourse.amitraul.finalApp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.tutorial.CircleTutorialActivity;

public class CirclesMainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private boolean firstTime=true;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstTime= !PreferenceManager.getDefaultSharedPreferences(getBaseContext()).contains("Circlefirst");
//        firstTime=false;
        if(firstTime) {

            setContentView(R.layout.activity_circles_main);
            findViewById(R.id.tutorial_layout).setVisibility(View.VISIBLE);
        }
        else {
             /*
            * No tutorial required
            * */
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }else{
                Intent intent = new Intent(this,MenuMapActivity.class);
                startActivity(intent);
            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(this,MenuMapActivity.class);
            startActivity(intent);

        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("location off");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("connection failed");
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                    Intent intent = new Intent(this,MenuMapActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(this, "Need your location to play the game!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    public void quitApp(View view){
        finish();
    }

    public void startTutorial(View view){
        Intent intent = new Intent(this, CircleTutorialActivity.class);
        startActivity(intent);
    }
}
