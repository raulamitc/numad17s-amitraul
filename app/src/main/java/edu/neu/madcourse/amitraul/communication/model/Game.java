package edu.neu.madcourse.amitraul.communication.model;

/**
 * Created by raula on 3/20/2017.
 */

public class Game {
    String firstUser= "";
    String secondUser="";
    String gameData ="";
    String scoreFirst= "0";
    String scoreSecond ="0";
    String gameTurn="";
    String time="";

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFirstUser() {
        return firstUser;
    }

    public void setFirstUser(String firstUser) {
        this.firstUser = firstUser;
    }

    public String getSecondUser() {
        return secondUser;
    }

    public void setSecondUser(String secondUser) {
        this.secondUser = secondUser;
    }

    public String getGameData() {
        return gameData;
    }

    public void setGameData(String gameData) {
        this.gameData = gameData;
    }

    public String getScoreFirst() {
        return scoreFirst;
    }

    public void setScoreFirst(String scoreFirst) {
        this.scoreFirst = scoreFirst;
    }

    public String getScoreSecond() {
        return scoreSecond;
    }

    public void setScoreSecond(String scoreSecond) {
        this.scoreSecond = scoreSecond;
    }

    public String getGameTurn() {
        return gameTurn;
    }

    public void setGameTurn(String gameTurn) {
        this.gameTurn = gameTurn;
    }
}
