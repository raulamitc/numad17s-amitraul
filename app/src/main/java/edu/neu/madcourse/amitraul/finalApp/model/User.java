package edu.neu.madcourse.amitraul.finalApp.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by raula on 4/20/2017.
 */

public class User {
    String email;
    String token;
    String name;
    HashMap<String,String> territories = new HashMap<>();
    String score;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getMyTerritories() {
        return  territories;
    }

    public void setMyTerritories(HashMap<String, String> myTerritories) {
        this. territories = myTerritories;
    }


    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
