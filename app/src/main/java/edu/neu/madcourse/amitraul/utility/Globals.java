package edu.neu.madcourse.amitraul.utility;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.BufferOverflowException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

import edu.neu.madcourse.amitraul.scroggle.ScroggleGameFragment;
import edu.neu.madcourse.amitraul.tricky.Accelerometer;

/**
 * Created by raula on 2/2/2017.
 */

public class Globals {
    private static final String TAG = "Globals file parsing";
    private static HashSet<String> words;
    private static HashSet<Integer> wordsInt;
    private static HashSet<Long> wordsLong;
    private static ArrayList<String> nineLetterWords;
    private static HashMap<Character, Integer> scores;
    private static GoogleApiClient mGoogleApiClient;

    public static int currentTime = ScroggleGameFragment.MAX_TIME;
    private static ArrayList<Accelerometer> accData;


    public static synchronized HashSet<String> getWords(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        if(words==null){
            words=new HashSet<>();
            if(nineLetterWords==null) nineLetterWords=new ArrayList<>();
            String line="";
            while ((line=in.readLine())!=null) {
                if(line.length()==9) nineLetterWords.add(line);
                words.add(line);
            }
        }
        return words;
    }

    public static synchronized ArrayList<Accelerometer> getAccelerometerData(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        if(accData==null){
            accData=new ArrayList<Accelerometer>();
            String line="";
            while ((line=in.readLine())!=null) {
                String[] ls= line.split(",");
                Accelerometer ac = new Accelerometer(Float.parseFloat(ls[0]),
                                                    Float.parseFloat(ls[1]),
                                                     Float.parseFloat(ls[2]));
                accData.add(ac);
            }
        }
        return accData;
    }

    public static ArrayList<String> getNineLetterWords(){
        return nineLetterWords;
    }

    public static HashSet<Integer> getWordsInt(InputStream inputStream) throws IOException {
        DataInputStream in = new DataInputStream(inputStream);
        if(wordsInt==null){
            wordsInt=new HashSet<>();
            while (in.available()!=0) {
                wordsInt.add(in.readInt());
            }
        }
        return wordsInt;
    }

    public static HashSet<Long> getWordsLong(InputStream inputStream) throws IOException {
        DataInputStream in = new DataInputStream(inputStream);
        if(wordsLong==null){
            wordsLong=new HashSet<>();
            while (in.available()!=0) {
                wordsLong.add(in.readLong());
            }
        }
        return wordsLong;
    }

    public static HashMap<Character,Integer> getTileScore() {
        
        if (scores==null) {
            scores=new HashMap<Character,Integer>();
            scores.put('A', 1);
            scores.put('B', 3);
            scores.put('C', 3);
            scores.put('D', 2);
            scores.put('E', 1);
            scores.put('F', 2);
            scores.put('G', 2);
            scores.put('H', 4);
            scores.put('I', 1);
            scores.put('J', 8);
            scores.put('K', 5);
            scores.put('L', 1);
            scores.put('M', 3);
            scores.put('N', 1);
            scores.put('O', 1);
            scores.put('P', 3);
            scores.put('Q', 10);
            scores.put('R', 1);
            scores.put('S', 1);
            scores.put('T', 1);
            scores.put('U', 1);
            scores.put('V', 4);
            scores.put('W', 4);
            scores.put('X', 8);
            scores.put('Y', 4);
            scores.put('Z', 10);
        }
        return scores;
    }

    public static GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public static void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        Globals.mGoogleApiClient = mGoogleApiClient;
    }

    /*
STEP-2 ::: Method to download the file from the Firebase Storage
 */
    private void downloadFile(String fileName,StorageReference storageRef) {
        Log.d(TAG, "downloadFile: " + fileName);
        StorageReference pathReference = storageRef.child(fileName);

        // FILE#1 - 10 minute dataset
//        StorageReference pathReference = storageRef.child("Crowdsourcing_test_(2017-03-08%5C)RAW_HPF.csv.gz");

        // FILE#2 - Existing annotated data set
//        StorageReference pathReference = storageRef.child("SPADESInLab.alvin-SPADESInLab.2015-10-08-14-10-41-252-M0400.annotation.csv.gz");

        // FILE#3 - 1 hour long dataset
//        StorageReference pathReference = storageRef.child("ActigraphGT9X-AccelerationCalibrated-NA.TAS1E23150066-AccelerationCalibrated.2015-10-08-14-00-00-000-M0400.sensor.csv.gz");

        // FILE#4 - 25 points dataset
//        StorageReference pathReference = storageRef.child("test.csv.gz");

        File localFile;
        try {
//            localFile = File.createTempFile("download.csv", "gz");
            localFile = File.createTempFile(pathReference.getName(), null);
            final File finalLocalFile = localFile;
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // Data for "gz file" is returned, use this as needed
                    Log.d(TAG, "onSuccess: File obtained..... SPACE of internal memory: " + finalLocalFile.getTotalSpace());
                    Log.d(TAG, "onSuccess: File obtained..... NAME: " + finalLocalFile.getName());
                    Log.d(TAG, "onSuccess: PATH: " + finalLocalFile.getPath());

                    // STEP-3 ::: Method to unzip the downloaded file
                    unzipFile(finalLocalFile.getParent(), finalLocalFile.getName());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Log.e(TAG, "onFailure: ", exception);
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                    Log.d(TAG, "onProgress: " + taskSnapshot.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    STEP-3 ::: Method to unzip the downloaded file
     */
    private void unzipFile(String path, String zipName) {
        String INPUT_GZIP_FILE = path + "/" + zipName;

        InputStream fis;
        GZIPInputStream gis;
        try {
            fis = new FileInputStream(INPUT_GZIP_FILE);
            gis = new GZIPInputStream(new BufferedInputStream(fis));

            Log.d(TAG, "gunzipFile: Gunzipping file");

            InputStreamReader reader = new InputStreamReader(gis);
            BufferedReader bufferReader = new BufferedReader(reader);

            // STEP-4a ::: Method to read data directly from compressed CSV file
            parser(bufferReader);

        } catch (IOException | BufferOverflowException e) {
            Log.e(TAG, "gunzipFile: ", e);
        }
    }

    /*
    STEP-4a ::: Method to read data directly from compressed CSV file
     */
    private void parser(BufferedReader bufferReader) {
        String line;
        try {
            int c = 0;
            String[] parts;
            while ((line = bufferReader.readLine()) != null) {
                parts = line.split(",");
                if (c > 0) {
//                    seriesX.add(milliSecond, Double.parseDouble(parts[1]));
//                    seriesY.add(milliSecond, Double.parseDouble(parts[2]));
//                    seriesZ.add(milliSecond, Double.parseDouble(parts[3]));
                }
                if (c == 40000) // c is used to plot upto first 40000 points
                    break;
                else
                    c++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
