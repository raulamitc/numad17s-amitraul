package edu.neu.madcourse.amitraul.finalApp.tutorial;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.MenuMapActivity;
import edu.neu.madcourse.amitraul.finalApp.TransparentActivity;
import edu.neu.madcourse.amitraul.tricky.Accelerometer;
import edu.neu.madcourse.amitraul.tricky.CircleDrawable;
import edu.neu.madcourse.amitraul.utility.Globals;

public class CircleTutorialActivity extends AppCompatActivity {
    private int[][] gameData = {{5200, 5300, 11000, 8000}, {5000, 12660, 2000, 13000}};
    private int[][] clickIndex = {{0, 1}, {1, 3}};
    public static ArrayList<Integer> images = new ArrayList<>(Arrays.asList(new Integer[]{R.id.tuimage1, R.id.tuimage2, R.id.tuimage3, R.id.tuimage4}));
    private ArrayList<Accelerometer> accData;
    private int game = 0;
    public static int tileNum = 0;
    private int max_games = 2;
    ImageView image1;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    GridLayout layout;

    ImageView screenshot1;
    Button nextButton;

    int screenshotCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_tutorial);
        try {
            accData = Globals.getAccelerometerData(getResources().openRawResource(R.raw.accelerometer_value));
        } catch (IOException e) {
            e.printStackTrace();
        }

        image1 = (ImageView) findViewById(R.id.tuimage1);
        image2 = (ImageView) findViewById(R.id.tuimage2);
        image3 = (ImageView) findViewById(R.id.tuimage3);
        image4 = (ImageView) findViewById(R.id.tuimage4);
        layout = (GridLayout) findViewById(R.id.circleGrid);
        screenshot1 = (ImageView) findViewById(R.id.screenshot1);
        nextButton = (Button) findViewById(R.id.scrrenshot_next_button);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tileNum == 0) loadGame();
        else if (tileNum > 0) {
            View vt = findViewById(CircleTutorialActivity.images.get(clickIndex[game - 1][tileNum - 1]));
            vt.setBackgroundColor(getResources().getColor(R.color.green_color));
            vt.setClickable(false);
        }
        if (game == max_games + 1) { // checks if number of games for tutorials are done

            // Show screenshot
            image1.setVisibility(View.GONE);
            image2.setVisibility(View.GONE);
            image3.setVisibility(View.GONE);
            image4.setVisibility(View.GONE);
            layout.setVisibility(View.GONE);

            screenshot1.setVisibility(View.VISIBLE);
            screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot1));
            nextButton.setVisibility(View.VISIBLE);
            screenshotCount += 1;
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (screenshotCount == 1) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot2));
                        screenshotCount += 1;
                    }
                    else if (screenshotCount == 2) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot3));
                        screenshotCount += 1;
                    }
                    else if (screenshotCount == 3) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot4));
                        screenshotCount += 1;
                    }
                    else if (screenshotCount == 4) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot5));
                        screenshotCount += 1;
                    }
                    else if (screenshotCount == 5) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot6));
                        screenshotCount += 1;
                    }
                    else if (screenshotCount == 6) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot7));
                        screenshotCount += 1;
                        //nextButton.setVisibility(View.GONE);
                    }
                    else if (screenshotCount == 7) {
                        screenshot1.setImageDrawable(getResources().getDrawable(R.drawable.screenshot8));
                        screenshotCount += 1;
                        //nextButton.setVisibility(View.GONE);
                    }
                    else if (screenshotCount == 8) {
                        nextButton.setVisibility(View.GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(CircleTutorialActivity.this);
                        builder.setMessage("Great Job! Now go ahead and match more circles to build your own territory and conquer colliding territories.");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Match More Circles",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(CircleTutorialActivity.this, MenuMapActivity.class);
                                        startActivity(intent);
                                    }
                                });
                        builder.show();
                    }
                }

            });
        } else {
            Intent intent = new Intent(this, TransparentActivity.class);
            intent.putExtra("index", clickIndex[game - 1][tileNum]);
            startActivity(intent);
        }
    }

    private void loadGame() {
        if (game == max_games) {
            game++;
            return;
        }
        Random rnd = new Random();
        int countCorrect = 2;
        String v = "";
        int i = 0;
        for (int image : images) {
            placeImage(image, gameData[game][i]);
            countCorrect--;
            i++;
        }
        game++;
    }

    public void placeImage(int image, int data) {
        ImageView button = (ImageView) findViewById(image);
        button.setImageDrawable(new CircleDrawable(accData, data));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundColor(getResources().getColor(R.color.green_color));
                v.setClickable(false);
                if (tileNum == 2) {
                    tileNum = 0;
                    loadGame();
                }
            }
        });
    }

    public void triggerOnClick(Integer index) {
        findViewById(images.get(index));
    }
}
