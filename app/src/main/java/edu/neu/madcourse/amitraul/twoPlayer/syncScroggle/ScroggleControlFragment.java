package edu.neu.madcourse.amitraul.twoPlayer.syncScroggle;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.amitraul.R;


public class ScroggleControlFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.sync_fragment_scroggle_control, container, false);
        View main = rootView.findViewById(R.id.button_main);
//        View restart = rootView.findViewById(R.id.button_restart);
        View mute = rootView.findViewById(R.id.button_mute);
        View pause = rootView.findViewById(R.id.button_pause);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ScroggleGameActivity) getActivity()).pauseTimer();
                getActivity().finish();
            }
        });
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ScroggleGameActivity) getActivity()).pauseMusic();
            }
        });
        return rootView;
    }

}
