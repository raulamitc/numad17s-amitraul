package edu.neu.madcourse.amitraul.finalApp.circleGame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import edu.neu.madcourse.amitraul.R;
import edu.neu.madcourse.amitraul.finalApp.firebaseAuth.ConnectToFirebaseDatabase;
import edu.neu.madcourse.amitraul.tricky.Accelerometer;
import edu.neu.madcourse.amitraul.tricky.CircleDrawable;
import edu.neu.madcourse.amitraul.utility.Globals;

public class CircleGameActivity extends AppCompatActivity {

    private ArrayList<Accelerometer> accData;
    int[] images= {R.id.image1,R.id.image2,R.id.image3,R.id.image4};
    HashMap<Integer,Integer> imageData=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_game);
        DatabaseReference ref = ConnectToFirebaseDatabase.instance(getApplicationContext()).getReference();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        try {
            accData= Globals.getAccelerometerData(getResources().openRawResource(R.raw.accelerometer_value));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Random rnd = new Random();
        for(int image: images) placeImage(image,rnd);
        Button refreshBtn = (Button) findViewById(R.id.refreshData);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageData.clear();
                for(int image: images) placeImage(image,new Random());
            }
        });

    }

    public void placeImage(int image,Random rnd){
        ImageView button = (ImageView) findViewById(image);
        int data=rnd.nextInt(accData.size()-200);
        imageData.put(image,data+100);
        button.setImageDrawable(new CircleDrawable(accData,data));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int data= imageData.get(v.getId());
                showLabelToast(data);
            }
        });
    }

    private void showLabelToast(int data) {
        String message="";
        if(790<=data && data<1519){
            message= "Walking";
        }else if (1816<=data && data<5116){
            message="Probably walking with bag";
        }else if ((5149<=data && data<5512)||(6634<=data && data<7380)||(10330<=data && data< 10627)){
            message="Shake the phone";
        }else if(7720<=data && data<8946){
            message="Jumping";
        }else if (8946<=data && data<9142){
            message= "Put the phone in the bag";
        }else if(9500<=data && data<9967){
            message="Carry the bag and walk";
        }else if(10070<=data && data<10330){
            message="Put the phone on the table for 1 second";
        }else if((11254<=data && data<11683)||(11954<=data && data<12310)){
            message="Put the phone in pocket and sit down";
        }else if((11683 <=data && data<11954)||(12310 <=data && data< 12550)){
            message="Stand up with phone in pocket";
        }else if(12550 <=data && data<13135){
            message="Hold the phone in hand and sit down";
        }else if(13201 <=data && data<14020){
            message="Stand up with phone in hand";
        }else{
            message="some other activity";
        }
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}