package edu.neu.madcourse.amitraul;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;

import edu.neu.madcourse.amitraul.communication.SignInActivity;
import edu.neu.madcourse.amitraul.scroggle.ScroggleMainActivity;
import edu.neu.madcourse.amitraul.testDict.TestDictActivity;
import edu.neu.madcourse.amitraul.ticTac.TicTacMainActivity;
import edu.neu.madcourse.amitraul.tricky.TrickyPartActivity;
import edu.neu.madcourse.amitraul.twoPlayer.TwoPlayerActivity;
import edu.neu.madcourse.amitraul.utility.Globals;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.neu.madcourse.amitraul.R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(edu.neu.madcourse.amitraul.R.id.toolbar);
        setSupportActionBar(toolbar);
        loadLongWords();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    private void loadLongWords() {
        try {
            Globals.getWords(getResources().openRawResource(R.raw.wordlist_en_str9));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == edu.neu.madcourse.amitraul.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void throwException(View view) throws Exception {
        throw new RuntimeException();
    }

    public void quitApp(View view) {
        System.exit(1);
    }


    public void getAboutMe(View view) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (MainActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Please Grant Permissions",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.READ_PHONE_STATE},
                                        PERMISSIONS_REQUEST_READ_PHONE_STATE);
                            }
                        }).show();
            } else if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE);
            } else {
                getAboutMe();
            }
        } else {
            getAboutMe();
        }
    }

    public void startTicTac(View view) {
        Intent intent = new Intent(this, TicTacMainActivity.class);
        startActivity(intent);
    }

    public void startScroggle(View view) {
        Intent intent = new Intent(this, ScroggleMainActivity.class);
        startActivity(intent);
    }

    public void startTwoPlayer(View view) {
        Intent intent = new Intent(this, TwoPlayerActivity.class);
        startActivity(intent);
    }

    public void startComm(View view) {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    public void startTestDict(View view) {
        Intent intent = new Intent(this, TestDictActivity.class);
        startActivity(intent);
    }

    public void getAboutMe() {
        Intent intent = new Intent(this, AboutMeActivity.class);
        startActivity(intent);
    }

    public void trickyPart(View view) {
        Intent intent = new Intent(this, TrickyPartActivity.class);
        startActivity(intent);
    }

    public void finalApp(View view){
        Intent intent = new Intent(this,FinalAppActivity.class);
        startActivity(intent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAboutMe();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}
